import * as React from 'react';
import {
  memo, FC, useCallback, useState,
} from 'react';
import { style } from 'typestyle';
import { createPortal } from 'react-dom';
import usePortal from '../../hooks/usePortal';
import useResponsive from '../../hooks/useResponsive';
import Carousel from '../Layouts/Carousel';
import Wall from '../Layouts/Wall';
import { Mosaic } from '../Layouts/Mosaic';
import PostViewer from '../PostViewer';
import { useStore } from '../../services/store';
import { IMedia } from '../../types';
import { Thumbnail } from '../Thumbnail';
import Visibility from '../Visibility';
import { Helpers } from '../../services/helpers';

interface WidgetProps {
  root: HTMLElement;
  startDate: Date;
  postViewerPortalId?: string;
}

// Maximum number of visible elements for carousels on mobile
const MAX_MOBILE_CAROUSEL_ELEMS = 3;

const Widget: FC<WidgetProps> = memo(({ root, startDate, postViewerPortalId }) => {
  const store = useStore();
  const [hasLoaded, setHasLoaded] = useState(false);

  const portalElement = usePortal(postViewerPortalId, !!store.post);

  useResponsive(root, (width) => {
    store.setStoreState((state) => ({
      ...state,
      isMobile: store.forceMobile || Helpers.isMobile(width),
    }));
  });

  const widgetLoaded = useCallback(() => {
    if (hasLoaded) return;

    const elapsedSeconds = startDate
      ? (new Date().getTime() - startDate.getTime()) / 1000
      : undefined;

    setHasLoaded(true);
    store.triggerEvent(
      'widgetLoaded',
      {
        elapsedSeconds,
        posts: store.data.content.medias,
        layout: store.data.settings.type,
      },
      store.data.id,
    );
  }, [hasLoaded, startDate, store]);

  const widgetLoadingFailed = useCallback(() => {
    if (hasLoaded) return;
    setHasLoaded(true);
    store.triggerEvent('widgetLoadingFailed', {}, store.data.id);
  }, [hasLoaded, store]);

  const mediaRender = useCallback(
    (media: IMedia, position: number) => <Thumbnail media={media} position={position} />,
    [],
  );

  if (!store.data) {
    widgetLoadingFailed();
    return null;
  }

  widgetLoaded();

  const currentType = store.data.settings.type || 'wall';
  const nbElems = store.isMobile
    ? Math.min(store.data.settings.content_size, MAX_MOBILE_CAROUSEL_ELEMS)
    : store.data.settings.content_size;

  return (
    <Visibility>
      {(ref) => (
        <div className={widgetClass} ref={ref}>
          <div className={`${store.config.classPrefix}-container`}>
            {
              {
                carousel: (
                  <Carousel
                    items={store.data.content.medias.map((m, i) => mediaRender(m, i))}
                    nbElems={nbElems}
                  />
                ),
                mosaic: <Mosaic />,
                wall: <Wall forceColumns={store.data.settings.forceColumns} />,
              }[currentType]
            }
          </div>
          {portalElement && store.post && createPortal(<PostViewer />, portalElement)}
        </div>
      )}
    </Visibility>
  );
});

const widgetClass = style({
  all: 'initial',
  $nest: {
    '*': {
      boxSizing: 'initial',
    },
  },
});

export default Widget;
