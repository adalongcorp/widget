/* eslint-disable max-len */
import * as React from 'react';
import { CSSProperties, HTMLAttributes, Component } from 'react';

export interface ArrowProps extends HTMLAttributes<HTMLOrSVGElement> {
  style?: CSSProperties;
  enabled?: boolean;
  size?: 'small' | 'medium' | 'large';
}

class LeftArrowIcon extends Component<ArrowProps> {
  public render() {
    const { style, enabled = true, size = 'medium', ...props } = this.props;
    const dynamicStyle = enabled ? arrowEnabled : arrowDisabled;

    const dimensions = {
      small: 29,
      medium: 41,
      large: 53,
    };

    return (
      <svg
        width={dimensions[size]}
        height={dimensions[size]}
        viewBox='0 0 41 41'
        style={{ ...arrowStyle, ...dynamicStyle, ...style }}
        {...props}
      >
        <circle
          cx='20.4254'
          cy='20.4255'
          r='19.9255'
          transform='rotate(-180 20.4254 20.4255)'
          fill='white'
          stroke='black'
        />
        <path
          d='M10.7895 19.6007C10.5552 19.835 10.5552 20.2149 10.7895 20.4492L14.6079 24.2676C14.8422 24.5019 15.2221 24.5019 15.4564 24.2676C15.6907 24.0333 15.6907 23.6534 15.4564 23.4191L12.0623 20.025L15.4564 16.6309C15.6907 16.3966 15.6907 16.0167 15.4564 15.7823C15.2221 15.548 14.8422 15.548 14.6079 15.7823L10.7895 19.6007ZM30.4377 19.425L11.2138 19.425L11.2138 20.625L30.4377 20.625L30.4377 19.425Z'
          fill='black'
        />
      </svg>
    );
  }
}

const arrowStyle: CSSProperties = {
  cursor: 'pointer',
};

const arrowEnabled: CSSProperties = {
  transition: 'opacity 0.3s ease-in-out, visibility 0.3s ease-in-out',
  opacity: 1,
};

const arrowDisabled: CSSProperties = {
  transition: 'opacity 0.3s ease-in-out, visibility 0.3s ease-in-out',
  opacity: 0,
  visibility: 'hidden',
};

export default LeftArrowIcon;
