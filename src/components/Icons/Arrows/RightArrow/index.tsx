/* eslint-disable max-len */
import * as React from 'react';
import { Component } from 'react';
import LeftArrowIcon, { ArrowProps } from '../LeftArrow';

class RightArrowIcon extends Component<ArrowProps> {
  public render() {
    const { style, ...props } = this.props;

    return <LeftArrowIcon style={{ transform: 'rotate(180deg)', ...style }} {...props} />;
  }
}

export default RightArrowIcon;
