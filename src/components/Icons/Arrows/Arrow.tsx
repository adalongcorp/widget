import * as React from 'react';
import { KeyboardEvent, MouseEvent, CSSProperties } from 'react';
import { classes } from 'typestyle';
import LeftArrowIcon from './LeftArrow';
import RightArrowIcon from './RightArrow';

interface ArrowProps {
  direction: 'left' | 'right';
  enabled: boolean;
  onClick: (e: MouseEvent<HTMLOrSVGElement> | KeyboardEvent<HTMLOrSVGElement>) => void;
  show: boolean;
  classPrefix: string | undefined;
  size?: 'small' | 'medium' | 'large';
}

const Arrow = ({ direction, enabled, onClick, show, classPrefix, size = 'medium' }: ArrowProps) => {
  const ArrowIcon = direction === 'left' ? LeftArrowIcon : RightArrowIcon;

  return (
    <div
      style={show ? getButtonStyle(size) : hiddenButton}
      className={classes(`${classPrefix}-arrow`, `${classPrefix}-arrow-${direction}`)}
    >
      <ArrowIcon
        enabled={enabled}
        onClick={onClick}
        onKeyDown={onClick}
        tabIndex={0}
        role='button'
        aria-label={direction === 'left' ? 'previous' : 'next'}
        size={size}
      />
    </div>
  );
};

const getButtonStyle = (size: 'small' | 'medium' | 'large'): CSSProperties => {
  const paddingSizes = {
    small: 4,
    medium: 8,
    large: 12,
  };

  return {
    flexShrink: 0,
    height: '100%',
    padding: paddingSizes[size],
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  };
};

const hiddenButton: CSSProperties = {
  ...getButtonStyle('medium'),
  visibility: 'hidden',
};

export default Arrow;
