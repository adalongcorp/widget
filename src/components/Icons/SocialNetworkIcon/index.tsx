/* eslint-disable max-len */
import * as React from 'react';
import { ReactNode, Component } from 'react';

export interface SocialNetworkIconProps {
  size?: number;
  color: string;
  children?: ReactNode;
}

class SocialNetworkIcon extends Component<SocialNetworkIconProps> {
  public render() {
    return (
      <svg
        version='1.1'
        viewBox='0 0 100 100'
        xmlSpace='preserve'
        style={{
          width: this.props.size || 12,
          verticalAlign: 'baseline',
          fill: this.props.color,
        }}
      >
        {this.props.children}
      </svg>
    );
  }
}

export default SocialNetworkIcon;
