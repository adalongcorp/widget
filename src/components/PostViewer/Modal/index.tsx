import * as React from 'react';
import {
  useState, useCallback, useRef, FC, MouseEvent, useContext
} from 'react';
import { keyframes, style } from 'typestyle';
import useBorderRadius from '../../../hooks/useBorderRadius';
import Arrow from '../../Icons/Arrows/Arrow';
import CloseIcon from '../../Icons/Close';
import { StoreContext, useStore } from '../../../services/store';
import { IMedia, Direction, ISettings, DisplayCustomShopThisLook } from '../../../types';
import Content from '../components/Content';
import ProductModal from '../components/Products/ProductModal';

interface ModalProps {
  post: IMedia;
  settings: ISettings;
  totalPosts: number;
  onClose: () => void;
  onPostChange: (post: IMedia | null) => void;
  classPrefix: string;
}

const Modal: FC<ModalProps> = ({
  post,
  totalPosts,
  onClose,
  onPostChange,
  settings,
  classPrefix,
}) => {
  const store = useStore();
  const context = useContext(StoreContext);
  const contentRef = useRef<HTMLDivElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  const [isClosing, setIsClosing] = useState(false);

  const firstPost = post.postIndex === 0;
  const lastPost = post.postIndex === totalPosts - 1;

  const baseSize = Math.min(window.innerHeight * 0.7, window.innerWidth * 0.7);
  const borderRadius = useBorderRadius(baseSize, baseSize, settings.thumbnail_corners);

  const isShopThisLookModalEnabled = settings.shop_this_look_display === 'modal';

  const handleArrowClick = (dir: Direction) => {
    const newIndex = dir === 'left' ? post.postIndex - 1 : post.postIndex + 1;
    onPostChange(store.data.content.medias[newIndex]);
  };

  const handleClose = useCallback(
    (e?: MouseEvent<HTMLElement | SVGElement | HTMLOrSVGElement>) => {
      e?.stopPropagation();
      setIsClosing(true);
      setTimeout(() => {
        onClose();
      }, 300);
    },
    [onClose],
  );

  return (
    <div className={styles.desktop} onClick={handleClose}>
      <div className={styles.containerWrapper} onClick={(e) => e.stopPropagation()}>
        <Arrow
          direction='left'
          enabled={!firstPost}
          onClick={() => handleArrowClick('left')}
          show={true}
          classPrefix={classPrefix}
        />

        <div
          ref={containerRef}
          className={`${classPrefix}-postviewer ${styles.container({
            borderRadius,
            shopThisLook: isShopThisLookModalEnabled,
            baseSize,
            isClosing,
          })}`}
        >
          <div className={styles.content(isShopThisLookModalEnabled)}>
            <Content ref={contentRef} />
          </div>

          {isShopThisLookModalEnabled && (
            <div className={styles.details(context.config.displayCustomShopThisLook)}>
              <ProductModal />
            </div>
          )}

          <CloseIcon
            className={`${classPrefix}-postviewer-close ${styles.closeButton}`}
            size={11}
            onClick={(e) => handleClose(e)}
          />
        </div>

        <Arrow
          direction='right'
          enabled={!lastPost}
          onClick={() => handleArrowClick('right')}
          show={true}
          classPrefix={classPrefix}
        />
      </div>
    </div>
  );
};

const styles = {
  desktop: style({
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    padding: 40,
    overflow: 'auto',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex: 1000,
  }),

  containerWrapper: style({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 'fit-content',
    maxWidth: '100%',
    margin: '0 auto',
    position: 'relative',
    gap: '20px',
  }),

  container: (props: {
    borderRadius: string;
    shopThisLook: boolean;
    baseSize: number;
    isClosing: boolean;
  }) => {
    const scaleUp = keyframes({
      from: {
        transform: 'scale(0.5)',
        opacity: 0,
      },
      to: {
        transform: 'scale(1)',
        opacity: 1,
      },
    });

    const scaleDown = keyframes({
      from: {
        transform: 'scale(1)',
        opacity: 1,
      },
      to: {
        transform: 'scale(0.5)',
        opacity: 0,
      },
    });

    return style({
      borderRadius: props.borderRadius,
      backgroundColor: 'white',
      position: 'relative',
      alignItems: 'center',
      width: props.shopThisLook ? props.baseSize * 2 : props.baseSize,
      height: props.baseSize,
      display: 'flex',
      animation: props.isClosing
        ? `${scaleDown} 0.3s ease-in forwards`
        : `${scaleUp} 0.3s ease-out forwards`,
    });
  },

  content: (shopThisLook: boolean) => style({
    display: 'flex',
    flexDirection: 'row',
    width: shopThisLook ? '50%' : '100%',
    height: '100%',
    overflow: 'hidden',
    position: 'relative',
    borderTopLeftRadius: 'inherit',
    borderTopRightRadius: shopThisLook ? 0 : 'inherit',
    borderBottomRightRadius: shopThisLook ? 0 : 'inherit',
    borderBottomLeftRadius: 'inherit',
  }),

  details: (customShopThisLook?: DisplayCustomShopThisLook) => style({
    width: '50%',
    height: '100%',
    overflow: typeof customShopThisLook === 'function' ? 'auto' : 'hidden',
  }),

  closeButton: style({
    position: 'absolute',
    top: 27,
    right: 20,
    cursor: 'pointer',
    zIndex: 2,
  }),
};

export default Modal;
