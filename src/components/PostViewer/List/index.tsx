import * as React from 'react';
import {
  FC, useState, useCallback, useRef, useEffect,
} from 'react';
import { keyframes, style } from 'typestyle';
import { useStore } from '../../../services/store';
import { ISettings, IMedia, Direction } from '../../../types';
import ProductList from '../components/Products/ProductList';
import Content from '../components/Content';
import CloseIcon from '../../Icons/Close';
import Arrow from '../../Icons/Arrows/Arrow';
import useBorderRadius from '../../../hooks/useBorderRadius';

interface ListProps {
  settings: ISettings;
  classPrefix: string;
  onClose: () => void;
  post: IMedia;
  totalPosts: number;
  onPostChange: (post: IMedia | null) => void;
}

const List: FC<ListProps> = ({
  settings,
  classPrefix,
  onClose,
  post,
  totalPosts,
  onPostChange,
}) => {
  const store = useStore();
  const containerRef = useRef<HTMLDivElement>(null);
  const contentRef = useRef<HTMLDivElement>(null);
  const { isMobile } = store;
  const [isClosing, setIsClosing] = useState(false);
  const [containerDimensions, setContainerDimensions] = useState({ width: 0, height: 0 });
  const borderRadius = useBorderRadius(
    containerDimensions.width,
    containerDimensions.height,
    settings.thumbnail_corners,
    isMobile ? 'top' : 'left',
  );
  const isShopThisLookEnabled = settings.shop_this_look_display !== 'none';
  const handleClose = useCallback(() => {
    setIsClosing(true);
    setTimeout(onClose, 300);
  }, [onClose]);

  const firstPost = post.postIndex === 0;
  const lastPost = post.postIndex === totalPosts - 1;

  const handleArrowClick = (dir: Direction) => {
    const newIndex = dir === 'left' ? post.postIndex - 1 : post.postIndex + 1;
    onPostChange(store.data.content.medias[newIndex]);
  };

  // observe container dimensions to adapt borderradius
  useEffect(() => {
    if (!containerRef.current) return;

    const resizeObserver = new ResizeObserver((entries) => {
      const { width, height } = entries[0].contentRect;
      setContainerDimensions({ width, height });
    });

    resizeObserver.observe(containerRef.current);

    // cleanup
    // eslint-disable-next-line consistent-return
    return () => resizeObserver.disconnect();
  }, []);

  return (
    <div
      ref={containerRef}
      className={`${classPrefix}-productlist 
      ${styles.container(isMobile, isShopThisLookEnabled, isClosing, borderRadius)}`}
    >
      <div className={styles.actionBar}>
        <div className={styles.actionButtons}>
          <Arrow
            direction='left'
            enabled={!firstPost}
            onClick={() => handleArrowClick('left')}
            show={true}
            classPrefix={classPrefix}
            size='small'
          />
          <Arrow
            direction='right'
            enabled={!lastPost}
            onClick={() => handleArrowClick('right')}
            show={true}
            classPrefix={classPrefix}
            size='small'
          />
          <CloseIcon
            className={`${styles.closeButton} ${classPrefix}-productlist-close`}
            size={11}
            onClick={handleClose}
          />
        </div>
      </div>

      {isShopThisLookEnabled ? (
        <div className={styles.productList}>
          <ProductList />
        </div>
      )
        : <div className={styles.content(isShopThisLookEnabled)}>
          <Content ref={contentRef} />
        </div>
      }
    </div>
  );
};

const styles = {
  container: (
    isMobile: boolean,
    shopThisLookEnabled: boolean,
    isClosing: boolean,
    borderRadius: string,
  ) => {
    const slideUp = keyframes({
      from: { transform: 'translateY(100%)' },
      to: { transform: 'translateY(0)' },
    });

    const slideDown = keyframes({
      from: { transform: 'translateY(0)' },
      to: { transform: 'translateY(100%)' },
    });

    const slideRight = keyframes({
      from: { transform: 'translateX(100%)' },
      to: { transform: 'translateX(0)' },
    });

    const slideLeft = keyframes({
      from: { transform: 'translateX(0)' },
      to: { transform: 'translateX(100%)' },
    });

    return style({
      position: 'fixed',
      bottom: 0,
      right: 0,
      height: isMobile ? (shopThisLookEnabled ? '57vh' : '70vh') : '100%',
      width: isMobile ? '100%' : '20%',
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: '#FFFFFF',
      borderRadius,
      zIndex: 1000,
      ...(isMobile
        ? {
          transform: 'translateY(100%)',
          animation: isClosing
            ? `${slideDown} 0.3s ease-in forwards`
            : `${slideUp} 0.3s ease-out forwards`,
        }
        : {
          transform: 'translateX(100%)',
          animation: isClosing
            ? `${slideLeft} 0.3s ease-in forwards`
            : `${slideRight} 0.3s ease-out forwards`,
        }
      ),
    });
  },

  actionBar: style({
    height: '7vh',
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
  }),

  actionButtons: style({
    display: 'flex',
    alignItems: 'center',
    zIndex: 1001,
  }),

  closeButton: style({
    margin: '0 12px',
    cursor: 'pointer',
  }),

  content: (isShopThisLook: boolean) => style({
    maxHeight: isShopThisLook ? '50%' : '100%',
    width: '100%',
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
  }),

  productList: style({
    flex: 1,
    minHeight: '42%',
    width: '100%',
  }),
};

export default List;
