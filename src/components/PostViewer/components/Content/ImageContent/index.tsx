import * as React from 'react';
import { FC, useContext, MouseEvent, KeyboardEvent } from 'react';
import { classes, style } from 'typestyle';
import UserName from '../../../../UserName';
import { StoreContext } from '../../../../../services/store';
import { onKeyDownEventCallback } from '../../../../../utils/onKeyDown';

interface ImageContentProps {
  image: string;
  post: string;
  source: string;
  textColor: string;
  contentClass: string;
  showDisplayName: boolean;
}

const ImageContent: FC<ImageContentProps> = ({
  image,
  post,
  source,
  textColor,
  contentClass,
  showDisplayName,
}) => {
  const context = useContext(StoreContext);

  const handlePostClick = (e: MouseEvent<HTMLAnchorElement> | KeyboardEvent<HTMLAnchorElement>) => {
    if (context.post) {
      context.triggerEvent(
        'originalPostOpened',
        {
          originalEvent: e.nativeEvent as Event,
          post: context.post,
        },
        context.data.id,
      );
    }
  };

  return (
    <>
      <div className={styles.imageContainer}>
        <div className={styles.imageFiller} style={{ backgroundImage: `url(${image})` }} />
        <div className={styles.imageWrapper}>
          <a
            href={post}
            target='_blank'
            className={styles.postLink}
            role='link'
            aria-label='go to the original post'
            rel='noreferrer'
            onClick={handlePostClick}
            onKeyDown={(e) => onKeyDownEventCallback(e, () => handlePostClick(e))}
          />
          <img
            src={image}
            className={classes(styles.content, contentClass)}
            alt={`${source} content`}
          />
          {showDisplayName && (
            <div className={styles.userName}>
              <UserName color={textColor} />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

const styles = {
  imageContainer: style({
    position: 'relative',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  postLink: style({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 2,
  }),
  userName: style({
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 3,
  }),
  imageWrapper: style({
    position: 'relative',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 'fit-content',
    maxHeight: '100%',
    width: 'fit-content',
    maxWidth: '100%',
    zIndex: 1,
  }),
  content: style({
    maxWidth: '100%',
    maxHeight: '100%',
    height: 'auto',
    width: 'auto',
    objectFit: 'contain',
    position: 'relative',
  }),
  videoContent: style({
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    overflow: 'hidden',
  }),
  playButton: style({
    position: 'absolute',
    cursor: 'pointer',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }),
  imageFiller: style({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
    zIndex: 1,
    backgroundRepeat: 'repeat',
    backgroundPosition: 'center',
    filter: 'blur(9px)',
    transform: 'scale(1.1)',
  }),
};

export default ImageContent;
