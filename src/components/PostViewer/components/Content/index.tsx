import * as React from 'react';
import { useRef, forwardRef, useContext, useMemo, CSSProperties } from 'react';
import { style } from 'typestyle';
import { StoreContext } from '../../../../services/store';
import useLuminance from '../../../../hooks/useLuminance';
import useVideoControls from '../../../../hooks/useVideoControls';
import ImageContent from './ImageContent';
import VideoContent from './VideoContent';

const useContentControls = (image: string | null, video: string | null) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const textColor = useLuminance(image || '', 0.9, 0.9, 0.5, true);
  const videoControls = useVideoControls(videoRef, video || '');

  return {
    videoRef,
    textColor,
    videoControls: video ? videoControls : null,
  };
};

const Content = forwardRef<HTMLDivElement>((_, contentRef) => {
  const context = useContext(StoreContext);
  const { image, video, post, username, source } = context.post!;

  const { videoRef, textColor, videoControls } = useContentControls(image ?? null, video ?? null);

  const contentClass = useMemo(
    () =>
      context.isMobile
        ? style({ width: '100%', height: '100%' })
        : style({ display: 'flex', flex: 1 }),
    [context.isMobile],
  );

  return (
    <div
      style={styles.container(context.isMobile)}
      className={`${context.config.classPrefix}-postviewer-content`}
      aria-label={`${source} content created by ${username}.`}
      tabIndex={0}
      ref={contentRef}
    >
      {video && videoControls ? (
        <VideoContent
          video={video}
          pause={videoControls.isPaused}
          onPause={videoControls.pauseVideo}
          onPlay={videoControls.playVideo}
          videoRef={videoRef}
          contentClass={contentClass}
          showDisplayName={context.data.settings.display_name}
        />
      ) : (
        image && (
          <ImageContent
            image={image}
            post={post}
            source={source}
            textColor={textColor}
            contentClass={contentClass}
            showDisplayName={context.data.settings.display_name}
          />
        )
      )}
    </div>
  );
});

const styles = {
  container: (isMobile: boolean): CSSProperties => ({
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    overflow: 'hidden',
    ...(isMobile && { height: '85vw' }),
    ...(!isMobile && { flex: 1 }),
  }),
};

export default Content;
