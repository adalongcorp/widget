import * as React from 'react';
import {
  RefObject, FC, useContext, MouseEvent, KeyboardEvent,
} from 'react';
import { style } from 'typestyle';
import { onKeyDownCallback, onKeyDownEventCallback } from '../../../../../utils/onKeyDown';
import PlayIcon from '../../../../Icons/Play';
import UserName from '../../../../UserName';
import { StoreContext } from '../../../../../services/store';

interface VideoContentProps {
  video: string;
  pause: boolean;
  onPause: () => void;
  onPlay: () => void;
  videoRef: RefObject<HTMLVideoElement>;
  contentClass: string;
  showDisplayName: boolean;
}

const VideoContent: FC<VideoContentProps> = ({
  video,
  pause,
  onPause,
  onPlay,
  videoRef,
  contentClass,
  showDisplayName,
}) => {
  const context = useContext(StoreContext);

  const handlePlay = (e: MouseEvent<HTMLDivElement> | KeyboardEvent<HTMLDivElement>) => {
    if (context.post) {
      context.triggerEvent(
        'videoPlayed',
        {
          originalEvent: e.nativeEvent as Event,
          post: context.post,
        },
        context.data.id,
      );
    }
    onPlay();
  };

  return (
    <>
      <video
        ref={videoRef}
        className={`${styles.videoContent} ${contentClass}`}
        controls={false}
        onClick={onPause}
        onKeyDown={(e) => onKeyDownCallback(e, onPause)}
        onEnded={onPause}
        tabIndex={0}
        role='button'
        aria-label='pause video'
      >
        <source src={video} type='video/mp4' />
      </video>
      {pause && (
        <div
          className={`${styles.playButton} ${context.config.classPrefix}-postviewer-play-video-btn`}
          onClick={handlePlay}
          onKeyDown={(e) => onKeyDownEventCallback(e, onPlay)}
          tabIndex={0}
          role='button'
          aria-label='play video'
        >
          <PlayIcon style={{ flex: 1 }} />
        </div>
      )}
      {showDisplayName && (
        <div className={styles.userName}>
          <UserName color='black' />
        </div>
      )}
    </>
  );
};

const styles = {
  videoContent: style({
    maxWidth: '100%',
    maxHeight: '100%',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    overflow: 'hidden',
  }),
  playButton: style({
    position: 'absolute',
    cursor: 'pointer',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }),
  userName: style({
    position: 'absolute',
    bottom: 10,
    right: 10,
    zIndex: 999,
  }),
};

export default VideoContent;
