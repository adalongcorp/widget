import filterExactOrSimilarProducts from '../helpers';
import { IProduct } from '../../../../../types';

describe('filterExactOrSimilarProducts', () => {
  const mockProducts: IProduct[] = [
    {
      id: '1',
      catalog_id: '1',
      company_id: '1',
      description: 'desc1',
      image_link: 'link1',
      link: 'link1',
      title: 'title1',
      updated_at: 'date1',
    },
    {
      id: '2',
      catalog_id: '2',
      company_id: '2',
      description: 'desc2',
      image_link: 'link2',
      link: 'link2',
      title: 'title2',
      updated_at: 'date2',
    },
    {
      id: '3',
      catalog_id: '3',
      company_id: '3',
      description: 'desc3',
      image_link: 'link3',
      link: 'link3',
      title: 'title3',
      updated_at: 'date3',
    },
  ];

  it('should return exact and similar products based on given IDs', () => {
    const result = filterExactOrSimilarProducts(2, 2, mockProducts, ['1'], ['2', '3']);
    expect(result.exactProducts).toEqual([mockProducts[0]]);
    expect(result.similarProducts).toEqual([mockProducts[1], mockProducts[2]]);
  });

  it('should respect maxExactProducts limit', () => {
    const result = filterExactOrSimilarProducts(1, 2, mockProducts, ['1', '2'], ['2', '3']);
    expect(result.exactProducts).toEqual([mockProducts[0]]);
    expect(result.similarProducts).toEqual([mockProducts[1], mockProducts[2]]);
  });

  it('should respect maxSimilarProducts limit', () => {
    const result = filterExactOrSimilarProducts(2, 1, mockProducts, ['1', '2'], ['2', '3']);
    expect(result.exactProducts).toEqual([mockProducts[0], mockProducts[1]]);
    expect(result.similarProducts).toEqual([mockProducts[1]]);
  });

  it('should handle null maxExactProducts and maxSimilarProducts as no limit', () => {
    const result = filterExactOrSimilarProducts(null, null, mockProducts, ['1', '2'], ['2', '3']);
    expect(result.exactProducts).toEqual([mockProducts[0], mockProducts[1]]);
    expect(result.similarProducts).toEqual([mockProducts[1], mockProducts[2]]);
  });

  it('should return empty arrays if no products are provided', () => {
    const result = filterExactOrSimilarProducts(2, 2, [], ['1'], ['2']);
    expect(result.exactProducts).toEqual([]);
    expect(result.similarProducts).toEqual([]);
  });

  it('should return empty arrays if no IDs are provided', () => {
    const result = filterExactOrSimilarProducts(2, 2, mockProducts, [], []);
    expect(result.exactProducts).toEqual([]);
    expect(result.similarProducts).toEqual([]);
  });

  it('should handle undefined products array', () => {
    const result = filterExactOrSimilarProducts(2, 2, undefined, ['1'], ['2']);
    expect(result.exactProducts).toEqual([]);
    expect(result.similarProducts).toEqual([]);
  });

  it('should handle undefined exactProductIds and similarProductIds', () => {
    const result = filterExactOrSimilarProducts(2, 2, mockProducts, undefined, undefined);
    expect(result.exactProducts).toEqual([]);
    expect(result.similarProducts).toEqual([]);
  });
});
