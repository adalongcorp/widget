import * as React from 'react';
import { FC } from 'react';
import { classes, style } from 'typestyle';

const Header: FC<{ isMobile: boolean; text: string }> = ({ isMobile, text }) => (
  <div className={styles.header}>
    <div className={classes(styles.title.main, isMobile && styles.title.mobile)}>{text}</div>
  </div>
);
const styles = {
  header: style({
    position: 'relative',
    padding: '9px 0',
    borderBottom: '1px solid black',
  }),

  title: {
    main: style({
      fontSize: 20,
    }),
    mobile: style({
      margin: 0,
    }),
  },
};

export default Header;
