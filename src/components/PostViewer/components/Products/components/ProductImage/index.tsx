import * as React from 'react';
import { FC, useEffect, useRef } from 'react';
import { useSetAtom } from 'jotai';
import { onKeyDownCallback } from '../../../../../../utils/onKeyDown';
import { ISettings } from '../../../../../../types';
import { productImageWidthAtom } from '../../../../../../atoms/productImageWidth';

interface ProductImageProps {
  imageLink?: string;
  title: string;
  cornerType: ISettings['thumbnail_corners'];
  borderRadius: string;
  onImageClick: () => void;
  containerSize: number;
  customButtons?: JSX.Element[] | undefined;
  productId: string;
}

const ProductImage: FC<ProductImageProps> = ({
  imageLink,
  title,
  cornerType,
  borderRadius,
  onImageClick,
  containerSize,
  customButtons,
  productId,
}) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const setAtomImageWidth = useSetAtom(productImageWidthAtom(productId));
  const lastContainerSize = useRef(containerSize);

  const handleImageLoad = (e: React.SyntheticEvent<HTMLImageElement>) => {
    const img = e.currentTarget;
    const aspectRatio = img.naturalWidth / img.naturalHeight;

    // Calculate width based on container constraints
    const containerHeight = img.parentElement?.clientHeight || containerSize;
    const scaledWidth = Math.min(containerSize, containerHeight * aspectRatio);

    setAtomImageWidth(scaledWidth);
  };

  useEffect(() => {
    if (containerRef.current && containerRef.current.clientHeight !== containerSize) {
      const img = containerRef.current.querySelector('img');
      if (img) {
        handleImageLoad({ currentTarget: img } as React.SyntheticEvent<HTMLImageElement>);
      }
    }
  }, [containerSize]);

  useEffect(() => {
    // update width when containerSize changes (even if image is already loaded)
    if (containerSize !== lastContainerSize.current) {
      lastContainerSize.current = containerSize;
      const img = containerRef.current?.querySelector('img');
      if (img && img.complete) { // Check if image is already loaded
        handleImageLoad({ currentTarget: img } as React.SyntheticEvent<HTMLImageElement>);
      }
    }
  }, [containerSize]);

  return (
  <div
    ref={containerRef}
    style={styles.imageContainer}
    onClick={onImageClick}
    onKeyDown={(e) => onKeyDownCallback(e, onImageClick)}
    tabIndex={0}
    role='link'
    aria-label={`go to the ${title}'s product page`}
  >
    {imageLink && (
      <img
        src={imageLink}
        alt={title}
        onLoad={handleImageLoad}
        style={{
          ...styles.image(containerSize),
          borderRadius: cornerType === 'rounded' ? borderRadius : '0px',
        }}
      />
    )}
    {customButtons}
  </div>);
};

const styles = {
  imageContainer: {
    display: 'block',
    position: 'relative' as const,
    cursor: 'pointer',
  },
  image: (containerSize: number) => ({
    display: 'block',
    maxWidth: containerSize,
    maxHeight: containerSize,
    width: 'auto',
    height: 'auto',
  }),
} as const;

export default ProductImage;
