import * as React from 'react';
import {
  forwardRef,
  useContext,
  createRef,
  useCallback,
  MouseEvent,
  KeyboardEvent,
  CSSProperties,
} from 'react';
import { stylesheet } from 'typestyle';
import { StoreContext } from '../../../../../../../services/store';
import { CustomProductButtons, IMedia, IProduct } from '../../../../../../../types';
import { onKeyDownEventCallback } from '../../../../../../../utils/onKeyDown';
import useBorderRadius from '../../../../../../../hooks/useBorderRadius';
import formatCurrency from '../../../../../../../utils/formatCurrency';
import ProductImage from '../../../components/ProductImage';
import Arrow from '../../../../../../Icons/Arrows/Arrow';

// TODO LEA make this dynamic based on window height
const ITEM_MAX_SIZE = 80;

interface ProductProps {
  product: IProduct;
  parentMedia: IMedia;
  isVisible: boolean;
}

const ProductListItem = forwardRef<HTMLDivElement, ProductProps>(
  ({ product, parentMedia, isVisible }, ref) => {
    const context = useContext(StoreContext);
    const anchorRef = createRef<HTMLAnchorElement>();

    const borderRadius = useBorderRadius(
      ITEM_MAX_SIZE,
      ITEM_MAX_SIZE,
      context.data.settings.thumbnail_corners,
    );

    const title = [product.title, product.variant_name].filter(Boolean).join(' - ');
    const imageLink = isVisible ? product.image_link : undefined;

    const handleShopClick = useCallback(
      (e: MouseEvent | KeyboardEvent) => {
        if (context.post) {
          context.triggerEvent(
            'shopThisLook',
            {
              post: context.post,
              originalEvent: e.nativeEvent,
              product: product.id,
            },
            context.data.id,
          );
        }

        if (context.config.shopThisLookHandler) {
          context.config.shopThisLookHandler({ media: parentMedia, product }, product.link);
        } else {
          window.open(product.link, '_self');
        }
      },
      [product, parentMedia, context],
    );

    const customButtons = context.config.getCustomProductButtons
      ? Object.entries(context.config.getCustomProductButtons(React, product, parentMedia)).map(
        ([position, Button]) => (
            <div
              key={position}
              className={styles.buttonPositions[position as keyof CustomProductButtons]}
              onClick={(e) => e.stopPropagation()}
            >
              <Button onClick={() => {}} />
            </div>
        ),
      )
      : undefined;

    return (
      <div
        ref={ref}
        style={styles.product}
        className={`${context.config.classPrefix}-postviewer-details-product`}
        onClick={handleShopClick}
        onKeyDown={(e) => onKeyDownEventCallback(e, () => handleShopClick(e))}
        role='button'
        tabIndex={0}
      >
        <div style={styles.imageContainer}>
          <ProductImage
            imageLink={imageLink}
            title={title}
            onImageClick={() => anchorRef.current?.click()}
            cornerType={context.data.settings.thumbnail_corners}
            borderRadius={borderRadius}
            customButtons={customButtons}
            containerSize={ITEM_MAX_SIZE}
            productId={product.id}
          />
        </div>

        <div style={styles.productInfo(context.isMobile)}>
          <div style={styles.title}>{title}</div>
          {product.price !== undefined && product.price > 0 && product.currency && (
            <div style={styles.price(context.isMobile)}>
              {formatCurrency(product.price, product.currency)}
            </div>
          )}
        </div>

        <div style={styles.arrowContainer}>
          <Arrow
            direction='right'
            size='small'
            show={true}
            enabled={true}
            classPrefix='product-list'
            onClick={() => handleShopClick}
          />
        </div>
      </div>
    );
  },
);

const styles = {
  product: {
    width: '100%',
    height: ITEM_MAX_SIZE,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    backgroundColor: '#FFFFFF',
    transition: 'background-color 0.2s',
    $nest: {
      '&:hover': {
        backgroundColor: '#F5F5F5',
      },
    },
  } as CSSProperties,

  imageContainer: {
    height: 'auto',
    width: 'auto',
    flexShrink: 0,
    display: 'flex',
    alignItems: 'center',
  } as CSSProperties,

  productInfo: (isMobile: boolean): CSSProperties => ({
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '0 15px',
    overflow: 'hidden',
    fontSize: isMobile ? 14 : 16,
  }),

  title: {
    fontWeight: 300,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    marginBottom: 5,
  } as CSSProperties,

  price: (isMobile: boolean): CSSProperties => ({
    fontSize: isMobile ? 12 : 14,
    color: '#666666',
  }),

  arrowContainer: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexShrink: 0,
  } as CSSProperties,

  buttonPositions: stylesheet({
    topRight: { position: 'absolute', top: 0, right: 0 },
    topLeft: { position: 'absolute', top: 0, left: 0 },
    bottomRight: { position: 'absolute', bottom: 0, right: 0 },
    bottomLeft: { position: 'absolute', bottom: 0, left: 0 },
  }),
};

export default ProductListItem;
