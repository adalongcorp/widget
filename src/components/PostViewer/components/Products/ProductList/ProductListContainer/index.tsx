import * as React from 'react';
import { FC, useRef } from 'react';
import { style } from 'typestyle';
import { IMedia, IProduct } from '../../../../../../types';
import ProductListItem from './ProductListItem';

interface ProductContainerProps {
  products: IProduct[];
  startIndex: number;
  onRefUpdate: (index: number, ref: HTMLDivElement | null) => void;
  parentMedia: IMedia;
}

const ProductListContainer: FC<ProductContainerProps> = ({
  products,
  startIndex,
  onRefUpdate,
  parentMedia,
}) => {
  const containerRef = useRef<HTMLDivElement>(null);

  return (
    <div ref={containerRef} className={styles.productContainer}>
      {products.map((product, i) => (
        <div
          key={product.id}
          className={styles.productLine}
          ref={(ref) => {
            onRefUpdate(startIndex + i, ref);
          }}
        >
          <ProductListItem
            isVisible={true}
            product={product}
            parentMedia={parentMedia}
          />
        </div>
      ))}
    </div>
  );
};

const styles = {
  productContainer: style({
    display: 'flex',
    flexDirection: 'column',
    gap: '10px',
    width: '100%',
    marginBottom: '10px',
  }),

  productLine: style({
    width: '100%',
    height: '10%',
    margin: '10px 0',
    minHeight: '80px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  }),
};

export default ProductListContainer;
