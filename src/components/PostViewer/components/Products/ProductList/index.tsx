import * as React from 'react';
import {
  useRef, useContext, useMemo, useEffect,
} from 'react';
import { style } from 'typestyle';
import { StoreContext } from '../../../../../services/store';
import filterExactOrSimilarProducts from '../helpers';
import Header from '../components/Header';
import ProductListContainer from './ProductListContainer';

export default function ProductList() {
  const context = useContext(StoreContext);
  const { products, productsLinked } = context.post!;
  const {
    data: { settings },
    config,
    isMobile,
  } = context;

  const containerRef = useRef<HTMLDivElement>(null);
  const productRefs = useRef<(HTMLDivElement | null)[]>([]);

  const { exactProducts, similarProducts } = useMemo(
    () => filterExactOrSimilarProducts(
      settings.max_exact_products_displayed,
      settings.max_similar_products_displayed,
      products,
      productsLinked.exact,
      productsLinked.similar,
    ),
    [settings, products, productsLinked],
  );

  useEffect(() => {
    productRefs.current = productRefs.current.slice(0, products?.length);
  }, [products]);

  if (config.displayCustomShopThisLook) {
    return config.displayCustomShopThisLook({
      react: React,
      products,
      media: context.post!,
      isMobile,
      context,
    });
  }

  const showProducts = settings.shop_this_look_display !== 'none' && (products?.length ?? 0) > 0;

  return (
    <>
      <div className={`${styles.container} ${config.classPrefix}-productlist`} ref={containerRef}>
        {showProducts && (
          <>
            {/* Case 1: Display only exact products */}
            {settings.display_exact_products_only && !settings.display_similar_products_only && (
              <>
                <Header isMobile={isMobile} text={config.shopThisLookText ?? 'Shop this look'} />
                <ProductListContainer
                  products={exactProducts}
                  startIndex={0}
                  onRefUpdate={(index, ref) => {
                    productRefs.current[index] = ref;
                  }}
                  parentMedia={context.post!}
                />
              </>
            )}

            {/* Case 2: Display only similar products */}
            {!settings.display_exact_products_only && settings.display_similar_products_only && (
              <>
                <Header isMobile={isMobile} text={config.moreLikeThisText ?? 'More like this'} />
                <ProductListContainer
                  products={similarProducts}
                  startIndex={0}
                  onRefUpdate={(index, ref) => {
                    productRefs.current[index] = ref;
                  }}
                  parentMedia={context.post!}
                />
              </>
            )}

            {/* Case 3: Display both exact and similar products */}
            {!settings.display_exact_products_only && !settings.display_similar_products_only && (
              <>
                {exactProducts.length > 0 && (
                  <>
                    <Header
                      isMobile={isMobile}
                      text={config.shopThisLookText ?? 'Shop this look'}
                    />
                    <ProductListContainer
                      products={exactProducts}
                      startIndex={0}
                      onRefUpdate={(index, ref) => {
                        productRefs.current[index] = ref;
                      }}
                      parentMedia={context.post!}
                    />
                  </>
                )}

                {similarProducts.length > 0 && (
                  <>
                    <Header
                      isMobile={isMobile}
                      text={config.moreLikeThisText ?? 'More like this'}
                    />
                    <ProductListContainer
                      products={similarProducts}
                      startIndex={exactProducts.length}
                      onRefUpdate={(index, ref) => {
                        productRefs.current[index] = ref;
                      }}
                      parentMedia={context.post!}
                    />
                  </>
                )}
              </>
            )}
          </>
        )}
      </div>
    </>
  );
}

const styles = {
  container: style({
    fontFamily: "'Fustat', sans-serif",
    color: '#231F20',
    fontSize: 16,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    overflowX: 'hidden',
    wordBreak: 'break-word',
    padding: 15,
    $nest: {
      '&::-webkit-scrollbar': {
        width: '5px',
        height: '100%',
      },
      '&::-webkit-scrollbar-track': {
        background: '#E5E5E5',
        height: '100%',
      },
      '&::-webkit-scrollbar-thumb': {
        background: '#000000',
        borderRadius: 0,
        border: 'none',
        minHeight: '40px',
      },
      '& > *': {
        width: '100%',
        minWidth: 0,
      },
    },
  }),
};
