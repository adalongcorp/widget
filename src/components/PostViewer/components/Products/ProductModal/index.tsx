import * as React from 'react';
import {
  useContext, useRef, useMemo, useEffect,
} from 'react';
import { style, classes } from 'typestyle';
import { StoreContext } from '../../../../../services/store';
import filterExactOrSimilarProducts from '../helpers';
import Header from '../components/Header';
import ProductModalContainer from './ProductModalContainer';

export default function ProductModal() {
  const context = useContext(StoreContext);
  const { products, productsLinked } = context.post!;
  const {
    data: { settings },
    config,
    isMobile,
  } = context;

  const containerRef = useRef<HTMLDivElement>(null);
  const productRefs = useRef<(HTMLDivElement | null)[]>([]);
  const { exactProducts, similarProducts } = useMemo(
    () => filterExactOrSimilarProducts(
      settings.max_exact_products_displayed,
      settings.max_similar_products_displayed,
      products,
      productsLinked.exact,
      productsLinked.similar,
    ),
    [settings, products, productsLinked],
  );

  useEffect(() => {
    productRefs.current = productRefs.current.slice(0, products?.length);
  }, [products]);

  if (config.displayCustomShopThisLook) {
    return config.displayCustomShopThisLook({
      react: React,
      products,
      media: context.post!,
      isMobile,
      context,
    });
  }

  const showProducts = settings.shop_this_look_display === 'modal' && (products?.length ?? 0) > 0;

  return (
    <div className={classes(styles.wrapper, `${config.classPrefix}-postviewer-details`)}>
      <div className={styles.container} ref={containerRef}>
        {showProducts && (
          <>
            {/* Case 1: Settings : Display only exact products */}
            {settings.display_exact_products_only && !settings.display_similar_products_only && (
              <>
                <Header isMobile={isMobile} text={config.shopThisLookText ?? 'Shop this look'} />
                <ProductModalContainer
                  products={exactProducts}
                  parentMedia={context.post!}
                  isSingleSection={true}
                />
              </>
            )}

            {/* Case 2: Settings : Display only similar products */}
            {!settings.display_exact_products_only && settings.display_similar_products_only && (
              <>
                <Header isMobile={isMobile} text={config.moreLikeThisText ?? 'More like this'} />
                <ProductModalContainer
                  products={similarProducts}
                  parentMedia={context.post!}
                  isSingleSection={true}
                />
              </>
            )}

            {/* Case 3: Settings : Display both exact and similar products */}
            {!settings.display_exact_products_only && !settings.display_similar_products_only && (
              <>
                {exactProducts.length > 0 ? (
                  <>
                    <Header
                      isMobile={isMobile}
                      text={config.shopThisLookText ?? 'Shop this look'}
                    />
                    <ProductModalContainer
                      products={exactProducts}
                      parentMedia={context.post!}
                      isSingleSection={similarProducts.length === 0}
                    />
                  </>
                ) : null}

                {similarProducts.length > 0 ? (
                  <>
                    <Header
                      isMobile={isMobile}
                      text={config.moreLikeThisText ?? 'More like this'}
                    />
                    <ProductModalContainer
                      products={similarProducts}
                      parentMedia={context.post!}
                      isSingleSection={exactProducts.length === 0}
                    />
                  </>
                ) : null}
              </>
            )}
          </>
        )}
      </div>
    </div>
  );
}

const styles = {
  wrapper: style({
    height: '100%',
    display: 'flex',
  }),

  container: style({
    fontFamily: "'Fustat', sans-serif",
    color: '#231F20',
    fontSize: 16,
    width: '100%',
    wordBreak: 'break-word',
    padding: '15px 25px',
    boxSizing: 'border-box',
  }),

  productLine: {
    margin: '30px 10px 45px 0',
    width: 'auto',
  },
};
