import * as React from 'react';
import {
  FC, useRef, useState, useEffect, useCallback
} from 'react';
import { style } from 'typestyle';
import useEmblaCarousel from 'embla-carousel-react';
import { IMedia, IProduct } from '../../../../../../types';
import ProductModalItem from './ProductModalItem';
import Arrow from '../../../../../Icons/Arrows/Arrow';

interface ProductContainerProps {
  products: IProduct[];
  parentMedia: IMedia;
  isSingleSection?: boolean;
}

const ProductModalContainer: FC<ProductContainerProps> = ({
  products,
  parentMedia,
  isSingleSection,
}) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [containerHeight, setContainerHeight] = useState<number>(0);

  const [canScrollPrev, setCanScrollPrev] = useState(false);
  const [canScrollNext, setCanScrollNext] = useState(false);

  const [emblaRef, emblaApi] = useEmblaCarousel({
    align: 'start',
    containScroll: false,
    dragFree: true,
    slidesToScroll: 'auto'
  });

  const scrollPrev = useCallback(() => {
    if (emblaApi) emblaApi.scrollPrev()
  }, [emblaApi])

  const scrollNext = useCallback(() => {
    if (emblaApi) emblaApi.scrollNext()
  }, [emblaApi])

  useEffect(() => {
    if (!emblaApi || !containerRef.current) return;
  
    // Reset scroll position when products change
    emblaApi.scrollTo(0);
  
    // Update scroll buttons and container height
    const updateState = () => {
      setCanScrollPrev(emblaApi.canScrollPrev());
      setCanScrollNext(emblaApi.canScrollNext());
    };
  
    // Handle resize events
    const resizeObserver = new ResizeObserver((entries) => {
      const { height } = entries[0].contentRect;
      setContainerHeight(height);
      emblaApi.reInit();
      updateState();
    });
  
    // Add event listeners
    window.addEventListener('resize', () => emblaApi.reInit());
    emblaApi.on('select', updateState);
    emblaApi.on('reInit', updateState);
    resizeObserver.observe(containerRef.current);
    
    // Initial state update
    updateState();
  
    // Cleanup
    return () => {
      window.removeEventListener('resize', () => emblaApi.reInit());
      emblaApi.off('select', updateState);
      emblaApi.off('reInit', updateState);
      resizeObserver.disconnect();
    };
  }, [emblaApi, products]);

  return (
    <div className={styles.carouselWrapper(isSingleSection)} ref={containerRef}>
      {canScrollPrev && (
        <button 
          className={styles.embla__prev} 
          onClick={scrollPrev}
          disabled={!canScrollPrev}
        >
          <Arrow
            direction='left'
            enabled={true}
            onClick={() => (null)}
            show={true}
            classPrefix='product-carousel'
            size='small'
          />
        </button>
      )}
  
      <div className={styles.embla}>
        <div className={styles.embla__viewport} ref={emblaRef}>
          <div className={styles.embla__container}>
            {products.map((product) => (
              <div key={product.id} className={styles.embla__slide}>
                <ProductModalItem
                  isVisible={true}
                  product={product}
                  parentMedia={parentMedia}
                  containerHeight={containerHeight}
                  isSingleSection={isSingleSection}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
  
      {canScrollNext && (
        <button 
          className={styles.embla__next} 
          onClick={scrollNext}
          disabled={!canScrollNext}
        >
          <Arrow
            direction='right'
            enabled={true}
            onClick={() => (null)}
            show={true}
            classPrefix='product-carousel'
            size='small'
          />
        </button>
      )}
    </div>
  )
};

  const styles = {
    carouselWrapper: (isSingleSection?: boolean) => style({
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: isSingleSection ? '90%' : '40%',
      margin: isSingleSection ? 'auto 0' : 0,
      gap: '10px' // Add space between arrows and carousel
    }),
  
    embla: style({
      overflow: 'hidden',
      flex: 1, // Take remaining space
      height: '100%',
      position: 'relative',
    }),
  
    embla__prev: style({
      backgroundColor: 'transparent',
      border: 'none',
      padding: 0,
      display: 'flex',
      alignItems: 'center',
    }),
  
    embla__next: style({
      backgroundColor: 'transparent',
      border: 'none',
      padding: 0,
      display: 'flex',
      alignItems: 'center',
    }),

  embla__container: style({
    display: 'flex',
    gap: '15px',
    alignItems: 'center',
    height: '100%',
  }),

  embla__slide: style({
    position: 'relative',
    flex: '0 0 auto',
    minWidth: '0',
    maxWidth: '300px',
  }),

  embla__viewport: style ({
    // need this class for embla to work
  }),
};

export default ProductModalContainer;
