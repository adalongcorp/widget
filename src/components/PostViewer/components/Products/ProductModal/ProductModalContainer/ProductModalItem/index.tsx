import * as React from 'react';
import {
  forwardRef,
  useContext,
  createRef,
  useCallback,
  MouseEvent,
  KeyboardEvent,
  CSSProperties,
} from 'react';
import { useAtomValue } from 'jotai';
import { stylesheet } from 'typestyle';
import { StoreContext } from '../../../../../../../services/store';
import { CustomProductButtons, IMedia, IProduct } from '../../../../../../../types';
import { onKeyDownEventCallback } from '../../../../../../../utils/onKeyDown';
import useBorderRadius from '../../../../../../../hooks/useBorderRadius';
import formatCurrency from '../../../../../../../utils/formatCurrency';
import ProductImage from '../../../components/ProductImage';
import { productImageWidthAtom } from '../../../../../../../atoms/productImageWidth';

const MIN_HEIGHT = 100;

interface ProductProps {
  product: IProduct;
  parentMedia: IMedia;
  isVisible: boolean;
  containerHeight: number;
  isSingleSection?: boolean;
}

const ProductModalItem = forwardRef<HTMLDivElement, ProductProps>(
  ({
    product, parentMedia, isVisible, containerHeight, isSingleSection,
  }, ref) => {
    const context = useContext(StoreContext);
    const anchorRef = createRef<HTMLAnchorElement>();

    const ITEM_MAX_SIZE = Math.min(
      Math.max(
        containerHeight * (isSingleSection ? 0.6 : 0.5), // 70% or 50% of container
        100, // minimum size
      ),
      250, // maximum size
    );

    const imageWidth = useAtomValue(productImageWidthAtom(product.id));
    const borderRadius = useBorderRadius(
      ITEM_MAX_SIZE,
      ITEM_MAX_SIZE,
      context.data.settings.thumbnail_corners,
    );

    const title = [product.title, product.variant_name].filter(Boolean).join(' - ');
    const imageLink = isVisible ? product.image_link : undefined;

    const handleShopClick = useCallback(
      (e: MouseEvent | KeyboardEvent) => {
        if (context.post) {
          context.triggerEvent(
            'shopThisLook',
            {
              post: context.post,
              originalEvent: e.nativeEvent,
              product: product.id,
            },
            context.data.id,
          );
        }

        if (context.config.shopThisLookHandler) {
          context.config.shopThisLookHandler({ media: parentMedia, product }, product.link);
        } else {
          window.open(product.link, '_self');
        }
      },
      [product, parentMedia, context],
    );

    const customButtons = context.config.getCustomProductButtons
      ? Object.entries(context.config.getCustomProductButtons(React, product, parentMedia)).map(
        ([position, Button]) => (
            <div
              key={position}
              className={styles.buttonPositions[position as keyof CustomProductButtons]}
              onClick={(e) => e.stopPropagation()}
            >
              <Button onClick={() => {}} />
            </div>
        ),
      )
      : undefined;

    return (
      <div
        ref={ref}
        style={styles.product(imageWidth, ITEM_MAX_SIZE)}
        className={`${context.config.classPrefix}-postviewer-details-product`}
      >
        <ProductImage
          containerSize={ITEM_MAX_SIZE}
          imageLink={imageLink}
          title={title}
          cornerType={context.data.settings.thumbnail_corners}
          borderRadius={borderRadius}
          onImageClick={() => anchorRef.current?.click()}
          customButtons={customButtons}
          productId={product.id}
        />

        <div style={styles.productInfo(context.isMobile)}>
          <a
            ref={anchorRef}
            onClick={handleShopClick}
            onKeyDown={(e) => onKeyDownEventCallback(e, () => handleShopClick(e))}
            tabIndex={0}
            role='link'
            aria-label={`go to the ${title}'s product page`}
            style={styles.title}
            target='_self'
          >
            {title}
          </a>
          {product.price !== undefined && product.price > 0 && product.currency && (
            <p style={styles.price(context.isMobile)}>
              {formatCurrency(product.price, product.currency)}
            </p>
          )}
        </div>
      </div>
    );
  },
);

const styles = {
  product: (width: number, itemMaxSize: number): CSSProperties => ({
    padding: '10px 10px 10px 0',
    marginTop: 10,
    display: 'flex',
    flexDirection: 'column' as const,
    width: width || 'fit-content',
    height: 'fit-content',
    minHeight: MIN_HEIGHT,
    maxWidth: itemMaxSize,
  }),
  productInfo: (isMobile: boolean): CSSProperties => ({
    display: 'flex',
    flexDirection: 'column',
    fontWeight: 300,
    fontSize: isMobile ? 14 : 16,
    color: 'black',
    margin: '10px 0 0',
  }),
  title: {
    wordBreak: 'break-word',
    display: '-webkit-box',
    WebkitLineClamp: 2,
    WebkitBoxOrient: 'vertical',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    cursor: 'pointer',
    margin: '5px 0',
  },
  price: (isMobile: boolean): CSSProperties => ({
    margin: 0,
    fontSize: isMobile ? 14 : 16,
  }),
  buttonPositions: stylesheet({
    topRight: { position: 'absolute', top: 0, right: 0 },
    topLeft: { position: 'absolute', top: 0, left: 0 },
    bottomRight: { position: 'absolute', bottom: 0, right: 0 },
    bottomLeft: { position: 'absolute', bottom: 0, left: 0 },
  }),
} as const;

export default ProductModalItem;
