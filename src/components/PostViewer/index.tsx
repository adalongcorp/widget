import * as React from 'react';
import { useEffect, useCallback, FC } from 'react';
import { style } from 'typestyle';
import { useStore } from '../../services/store';
import { Helpers } from '../../services/helpers';
import { IMedia } from '../../types';
import List from './List';
import Modal from './Modal';

const PostViewer: FC = () => {
  const store = useStore();

  // Event handlers
  const handlePostChange = useCallback(
    (post: IMedia | null) => {
      if (post) {
        store.setStoreState((state) => ({ ...state, post }));
        store.triggerEvent('postOpened', { post }, store.data.id);
        Helpers.replacePostInHistory(post);
      }
    },
    [store],
  );

  const handleClose = useCallback(() => {
    if (store.post) {
      store.triggerEvent('postClosed', { post: store.post }, store.data.id);
    }
    store.setStoreState((prevState) => ({ ...prevState, post: null }));
    window.history.replaceState(null, '', null);
  }, [store]);

  // Basic keyboard navigation
  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Escape') handleClose();
    };
    document.addEventListener('keydown', handleKeyDown);
    return () => document.removeEventListener('keydown', handleKeyDown);
  }, [handleClose]);

  if (!store.post) return null;

  const shouldShowList = store.isMobile
  || store.data.settings.shop_this_look_display === 'product_list';

  return (
    <div className={styles.wrapper}>
      <div className={styles.overlay} onClick={handleClose} />
      {shouldShowList ? (
      <List
        post={store.post}
        settings={store.data.settings}
        totalPosts={store.data.content.medias.length}
        onClose={handleClose}
        onPostChange={handlePostChange}
        classPrefix={store.config.classPrefix || ''}
      />
      ) : (
      <Modal
        post={store.post}
        settings={store.data.settings}
        totalPosts={store.data.content.medias.length}
        onClose={handleClose}
        onPostChange={handlePostChange}
        classPrefix={store.config.classPrefix || ''}
      />
      )}
    </div>
  );
};

const styles = {
  wrapper: style({
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 998,
  }),

  overlay: style({
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex: 999,
  }),
};

export default PostViewer;
