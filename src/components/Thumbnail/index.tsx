import * as React from 'react';
import {
  FC, useState, useCallback, MouseEvent, KeyboardEvent, useRef, useEffect
} from 'react';
import { style } from 'typestyle';
import { EventMap, IMedia, ISettings } from '../../types';
import { useStore } from '../../services/store';
import PlayIcon from '../Icons/Play';
import { Helpers } from '../../services/helpers';
import { onKeyDownEventCallback } from '../../utils/onKeyDown';
import useBorderRadius from '../../hooks/useBorderRadius';
import UserName from '../UserName';
import useLuminance from '../../hooks/useLuminance';
import { Packshot } from './Packshot';

const ASPECT_RATIOS = {
  square: 100,
  portrait: 125,
  stories: 177.78,
} as const;

export const Thumbnail: FC<{ media: IMedia; position: number }> = ({ media, position }) => {
  const store = useStore();
  const [imageVisible, setImageVisible] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const thumbnailRef = useRef<HTMLDivElement>(null);
  const luminanceColor = useLuminance(media.image, 0.9, 0.1, 0.5, isLoaded);
  const textColor = isLoaded ? luminanceColor : '#ffffff';

  const {
    thumbnail_corners: cornerType,
    thumbnail_shape: thumbnailShape = 'square',
    thumbnail_packshot: thumbnailPackshot,
  } = store.data.settings;
  const thumbnailImage = media.type === 'video' && media.thumbnail ? media.thumbnail : media.image;
  const borderRadius = useBorderRadius(100, 100, store.data.settings.thumbnail_corners);

  const handleEvent = useCallback(
    (eventName: keyof EventMap, originalEvent?: Event) => {
      store.triggerEvent(eventName, { post: media, position, originalEvent }, store.data.id);
    },
    [media, position, store],
  );

  const handleImageLoad = useCallback(() => {
    handleEvent('thumbnailLoaded');
  }, [handleEvent]);
  
  const handleOpen = useCallback(
    (e: MouseEvent | KeyboardEvent) => {
      handleEvent('thumbnailClicked', e.nativeEvent);
      handleEvent('postOpened');
      store.setStoreState((state) => ({ ...state, post: media }));
      Helpers.pushPostToHistory(media);
    },
    [handleEvent, media, store],
  );

  const handleImageError = useCallback(() => {
    handleEvent('thumbnailUnavailable');
    store.setStoreState((state) => ({
      ...state,
      data: {
        ...state.data,
        content: {
          ...state.data.content,
          medias: state.data.content.medias
            .filter((_, idx) => idx !== media.postIndex)
            .map((m, idx) => ({ ...m, postIndex: idx })),
        },
      },
    }));
  }, [handleEvent, media.postIndex, store]);

// Sets up intersection observer to track thumbnail visibility (for lazy loading)
useEffect(() => {
  // Create observer instance that triggers when thumbnail enters/exits viewport
  const observer = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          setImageVisible(true); 
          observer.unobserve(entry.target); // stop since we don't need updates
        }
      });
    },
    {
      root: null, // use viewport as root
      rootMargin: '0px', // no margin around viewport
      threshold: 0.1 // trigger when 10% visible
    }
  );

  if (thumbnailRef.current) {
    observer.observe(thumbnailRef.current);
  }

  return () => {
    if (thumbnailRef.current) {
      observer.unobserve(thumbnailRef.current);
    }
  };
}, []);

// Handles image creation + loading once thumbnail becomes visible
useEffect(() => {
  if (imageVisible && !isLoaded && thumbnailImage) {
    const img = new Image();
    img.src = thumbnailImage;
    
    img.onload = () => {
      setIsLoaded(true); 
      handleEvent('thumbnailLoaded'); 
    };
    
    img.onerror = handleImageError;

    return () => {
      img.onload = null;
      img.onerror = null;
    };
  }
}, [imageVisible, thumbnailImage, isLoaded, handleEvent, handleImageError]);

  return (
    <div
      ref={thumbnailRef}
      data-url={media.post}
      data-position={position}
      className={`${store.config.classPrefix}-thumbnail`}
      style={styles.thumbnail(thumbnailShape)}
      onClick={handleOpen}
      onKeyDown={(e) => onKeyDownEventCallback(e, handleOpen)}
      tabIndex={0}
      role='link'
      aria-label={`open the preview of the ${media.source} content created by ${media.username}`}
      onMouseEnter={() => handleEvent('thumbnailHover')}
    >
      <div className={containerClass}>
        <div
          style={{
            ...styles.image(borderRadius),
            ...(isLoaded && {
              backgroundImage: `url("${thumbnailImage}")`,
            }),
          }}
        >
          {media.type === 'video' && <PlayIcon size={30} style={{ flex: 1 }} />}
          {media.username && isLoaded ? (
            <div data-username={media.username} className={userNameClass}>
              <UserName
                color={textColor}
                username={media.username}
                source={media.source}
                postUrl={media.post}
              />
            </div>
          ) : null}
          {thumbnailPackshot && media.products && media.products.length > 0 && isLoaded ? (
            <Packshot products={media.products} shape={thumbnailShape} cornerType={cornerType} />
          ) : null}
        </div>
      </div>

      {imageVisible ? (
        <img
          src={thumbnailImage}
          alt={`${media.source} content created by ${media.username}`}
          onError={handleImageError}
          onLoad={handleImageLoad}
          style={{ display: 'none' }}
        />
      ) : null}
    </div>
  );
};

const styles = {
  thumbnail: (shape: ISettings['thumbnail_shape']) => ({
    paddingTop: `${ASPECT_RATIOS[shape]}%`,
    width: '100%',
    position: 'relative' as const,
    cursor: 'pointer',
  }),

  image: (cornerRadius: string) => ({
    position: 'relative' as const,
    flex: 1,
    margin: 4,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundColor: '#282828',
    display: 'flex',
    flexDirection: 'column' as const,
    alignItems: 'center',
    overflow: 'hidden',
    borderRadius: cornerRadius,
  }),
} as const;

const containerClass = style({
  position: 'absolute',
  height: '100%',
  width: '100%',
  top: 0,
  left: 0,
  display: 'flex',
  $nest: {
    '&:hover .username': {
      opacity: 1,
    },
    // TODO lea : Define with clémence if hover on thumbnail or visible packshot
    // '&:hover .packshot': {
    //   backgroundColor: 'rgba(255, 255, 255, 0.2)',
    //   backdropFilter: 'blur(20px)',
    //   '-webkit-backdrop-filter': 'blur(20px)',
    // },
  },
});

const userNameClass = `${style({
  position: 'absolute',
  zIndex: 3,
  top: 5,
  right: 5,
  opacity: 0,
  transition: 'opacity 0.2s ease',
})} username`; // we use this to target the username in the thumbnail for hover styling

export default Thumbnail;
