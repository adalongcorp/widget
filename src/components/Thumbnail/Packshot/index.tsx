import * as React from 'react';
import { FC, useRef, useState, useEffect, useContext } from 'react';
import { style } from 'typestyle';
import useBorderRadius from '../../../hooks/useBorderRadius';
import { ISettings, IProduct } from '../../../types';
import { StoreContext } from '../../../services/store';

interface PackshotProps {
  products: IProduct[];
  shape: ISettings['thumbnail_shape'];
  cornerType: ISettings['thumbnail_corners'];
}

const getHeightPercentage = (shape: ISettings['thumbnail_shape']) => {
  if (shape === 'square') return 27;
  if (shape === 'portrait') return 24;
  return 20;
};

export const Packshot: FC<PackshotProps> = ({ products, shape, cornerType }) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [containerHeight, setContainerHeight] = useState(0);
  const context = useContext(StoreContext); // Add this line to get access to context

  const handlePackshotClick = (e: React.MouseEvent, product: IProduct) => {
    // Trigger the packshot clicked event
    if (context.post) {
      context.triggerEvent(
        'packshotClicked',
        {
          post: context.post,
          originalEvent: e.nativeEvent,
          product: product.id,
        },
        context.data.id,
      );
    }
    window.open(product.link, '_blank');
  };

  // observe container height (to calculate border radius based on image size)
  useEffect(() => {
    if (!containerRef.current) return;

    const updateSize = () => {
      const height = containerRef.current?.clientHeight || 0;
      setContainerHeight(height);
    };

    const resizeObserver = new ResizeObserver(updateSize);
    resizeObserver.observe(containerRef.current);
    updateSize();

    // eslint-disable-next-line consistent-return
    return () => resizeObserver.disconnect();
  }, []);

  // calculate image size from container height (90%)
  const imageSizePx = Math.floor(containerHeight * 0.9);

  const borderRadius = useBorderRadius(imageSizePx, imageSizePx, cornerType);

  if (!products?.length) return null;

  return (
    <div ref={containerRef} className={styles.packshotContainer(shape)}>
      {products.slice(0, 4).map((product) => (
        <div key={product.id} className={styles.productWrapper}>
          <img
            src={product.image_link}
            alt={product.title}
            style={styles.productImageStyle(borderRadius)}
            onClick={(e) => handlePackshotClick(e, product)}
          />
        </div>
      ))}
    </div>
  );
};

const styles = {
  packshotContainer: (shape: ISettings['thumbnail_shape']) =>
    `${style({
      position: 'absolute',
      zIndex: 2,
      bottom: 0,
      left: 0,
      right: 0,
      height: `${getHeightPercentage(shape)}%`,
      backgroundColor: 'rgba(255, 255, 255, 0.1)',
      backdropFilter: 'blur(6px)',
      '-webkit-backdrop-filter': 'blur(6px)',
      display: 'flex',
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'flex-start',
      gap: '2px',
      padding: '4px',
      transition: 'all 0.2s ease',
      $nest: {
        '&:hover': {
          backgroundColor: 'rgba(255, 255, 255, 0.2)',
          backdropFilter: 'blur(20px)',
          '-webkit-backdrop-filter': 'blur(20px)',
        },
      },
    })} packshot`, // we use this to target the container in the thumbnail for hover styling

  productWrapper: style({
    flex: '1 1 0',
    minWidth: 0,
    maxWidth: '22.5%', // 4 products max (90/4)
    aspectRatio: '1', // square ratio
    position: 'relative',
    marginLeft: '4px',
  }),

  productImageStyle: (borderRadius: string) =>
    ({
      width: '90%',
      height: '90%',
      objectFit: 'cover',
      borderRadius,
      cursor: 'pointer',
      margin: '4px',
    } as const),
};

export default Packshot;
