import * as React from 'react';
import {
  CSSProperties,
  useContext,
  useRef,
  useState,
  UIEvent,
  MouseEvent,
  KeyboardEvent,
  useEffect,
} from 'react';
import Arrow from '../../Icons/Arrows/Arrow';
import { StoreContext } from '../../../services/store';
import { Direction } from '../../../types';

interface Props<T> {
  items: T[];
  nbElems: number;
  style?: CSSProperties;
}

interface State {
  slidePosition: number;
  squareSize: number;
  canSlideLeft: boolean;
  canSlideRight: boolean;
}

const Carousel = (props: Props<JSX.Element>) => {
  const context = useContext(StoreContext);
  const slideRef = useRef<HTMLDivElement>(null);
  let lastScrollValue = 0;
  let lastSlidePosition = 0;
  const [carouselState, setCarouselState] = useState<State>({
    slidePosition: 0, // index of the first visible content
    squareSize: 0,
    canSlideLeft: false,
    canSlideRight: props.items.length > props.nbElems,
  });

  useEffect(() => {
    if (slideRef.current) {
      calculateSquareSize();
    }
  }, [slideRef, slideRef.current]);

  useEffect(() => {
    context.setStoreState((state) => ({
      ...state,
      canSlideLeft: false,
      canSlideRight: props.items.length > props.nbElems,
    }));
  }, []);

  const carouselStyles = {
    ...carouselStyle,
    ...props.style,
  };

  useEffect(() => {
    window.addEventListener('resize', () => calculateSquareSize(true));
    return () => {
      window.removeEventListener('resize', () => calculateSquareSize(true));
    };
  }, []);

  useEffect(() => {
    const evtListener = ({ detail }: { detail: Direction }) => {
      onArrowClick(undefined, detail, carouselState);
    };
    // @ts-ignore
    window.addEventListener('adalongWidget_slide', evtListener);
    return () => {
      // @ts-ignore
      window.removeEventListener('adalongWidget_slide', evtListener);
    };
  }, [carouselState]);

  useEffect(() => {
    const maxPosition = props.items.length - numElems() + 1;
    const boundedPosition = Math.min(maxPosition, carouselState.slidePosition);
    const slidePosition = Math.max(boundedPosition, 0);
    setCarouselState({
      squareSize: carouselState.squareSize,
      slidePosition,
      canSlideLeft: slidePosition > 0,
      canSlideRight: slidePosition < props.items.length - props.nbElems,
    });
    context.setStoreState((state) => ({
      ...state,
      canSlideLeft: slidePosition > 0,
      canSlideRight: slidePosition < props.items.length - props.nbElems,
    }));
  }, [props.items.length]);

  const renderNativeScroll = () => (
    <div style={nativeScrollSlideStyle} ref={slideRef} onScroll={onMobileScroll}>
      <div style={nativeScrollSlideContainerStyle}>{renderMedia()}</div>
    </div>
  );

  function displayArrowNavorNativeScroll() {
    if (context.isMobile) {
      return context.config.displayMobileCarouselArrows
        ? renderArrowNavigation()
        : renderNativeScroll();
    }
    return renderArrowNavigation();
  }

  const renderArrowNavigation = () => {
    const showArrows = props.nbElems < props.items.length && !context.config.hideArrows;
    return (
      <>
        <Arrow
          direction='left'
          enabled={carouselState.canSlideLeft}
          onClick={(e) => onArrowClick(e, 'left')}
          show={showArrows}
          classPrefix={context.config.classPrefix}
        />

        <div style={desktopSlideStyle} ref={slideRef}>
          <div
            style={{
              ...desktopSlideContainerStyle,
              left: -(carouselState.slidePosition * carouselState.squareSize),
            }}
          >
            {renderMedia()}
          </div>
        </div>

        <Arrow
          direction='right'
          enabled={carouselState.canSlideRight}
          onClick={(e) => onArrowClick(e, 'right')}
          show={showArrows}
          classPrefix={context.config.classPrefix}
        />
      </>
    );
  };

  const renderMedia = (): JSX.Element[] => props.items.map((media, i) => (
      <div
        style={{
          ...thumbnailStyle,
          width: carouselState.squareSize,
        }}
        key={i}
      >
        {media}
      </div>
  ));

  function numElems(): number {
    if (!slideRef.current) {
      return props.nbElems;
    }

    const minWidth = 125;
    // Display minimum one media
    const isWideEnough = slideRef.current.clientWidth / props.nbElems > minWidth;
    return isWideEnough ? props.nbElems : Math.floor(slideRef.current.clientWidth / minWidth) || 1;
  }

  // In calculateSquareSize function, modify to use width only
  function calculateSquareSize(resize?: boolean): number {
    if (slideRef.current) {
      const width = slideRef.current.clientWidth / numElems();
      const squareSize = !context.isMobile ? width : 0.9 * width;

      if (carouselState.squareSize === 0 || resize) {
        setCarouselState((state) => ({
          ...state,
          squareSize,
        }));
      }
      return squareSize;
    }
    return 0;
  }

  const onMobileScroll = (event: UIEvent<HTMLDivElement>) => {
    let dir: 'left' | 'right';
    const scrollValue = event.currentTarget.scrollLeft;
    const slidePosition = Math.floor(scrollValue / carouselState.squareSize);

    if (!slideRef.current) {
      return;
    }

    if (scrollValue > lastScrollValue) {
      dir = 'left';
    } else {
      dir = 'right';
    }

    lastScrollValue = scrollValue;

    if (lastSlidePosition !== slidePosition) {
      lastSlidePosition = slidePosition;

      context.triggerEvent(
        'carouselNativeScroll',
        {
          direction: dir,
          position: lastSlidePosition,
          originalEvent: event.nativeEvent,
        },
        context.data.id,
      );
    }
  };

  type ArrowClickEvent = MouseEvent<HTMLOrSVGElement> | KeyboardEvent<HTMLOrSVGElement> | undefined;

  function onArrowClick(
    event: ArrowClickEvent,
    dir: 'right' | 'left',
    state?: State,
    shift?: number,
  ) {
    if (!slideRef.current) {
      return;
    }

    const max = props.items.length;
    const width = slideRef.current.clientWidth;

    let { slidePosition } = state ?? carouselState;
    const { squareSize, canSlideRight, canSlideLeft } = state ?? carouselState;

    const move = Math.floor(width / squareSize);

    if (dir === 'right' && canSlideRight) {
      slidePosition = shift ? shift + move : slidePosition + move;
    } else if (dir === 'left' && canSlideLeft) {
      slidePosition = shift ? shift - move : slidePosition - move;
    }
    slidePosition = Math.max(0, Math.min(max - move, slidePosition));
    setCarouselState({
      squareSize: carouselState.squareSize,
      slidePosition,
      canSlideLeft: slidePosition > 0,
      canSlideRight: slidePosition < max - move,
    });
    context.setStoreState((prevState) => ({
      ...prevState,
      canSlideLeft: slidePosition > 0,
      canSlideRight: slidePosition < max - move,
    }));
    if (event) {
      context.triggerEvent(
        'carouselArrowClicked',
        {
          position: slidePosition,
          originalEvent: event.nativeEvent,
          direction: dir,
        },
        context.data.id,
      );
    }
  }

  return (
    <div className={`${context.config.classPrefix}-layout-carousel`} style={carouselStyles}>
      {displayArrowNavorNativeScroll()}
    </div>
  );
};

const carouselStyle: CSSProperties = {
  width: '100%',
  display: 'flex',
  position: 'relative',
  minHeight: 'fit-content',
  alignItems: 'center',
};

const desktopSlideStyle: CSSProperties = {
  flex: 1,
  height: 'auto',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  position: 'relative',
  display: 'flex',
  alignItems: 'center',
};

const desktopSlideContainerStyle: CSSProperties = {
  position: 'relative',
  height: 'auto',
  top: 0,
  left: 0,
  transition: 'left 0.5s',
  display: 'flex',
};

const nativeScrollSlideStyle: CSSProperties = {
  height: 'auto',
  minWidth: '100%',
  overflowX: 'scroll',
  overflowY: 'hidden',
  whiteSpace: 'nowrap',
  position: 'relative',
  display: 'flex',
};

const nativeScrollSlideContainerStyle: CSSProperties = {
  position: 'relative',
  height: 'auto',
  display: 'flex',
};

const thumbnailStyle: CSSProperties = {
  display: 'inline-block',
  height: 'auto',
  verticalAlign: 'top',
};

export default Carousel;
