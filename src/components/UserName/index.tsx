import * as React from 'react';
import { FC, CSSProperties, useContext, MouseEvent, KeyboardEvent } from 'react';
import { style } from 'typestyle';
import { StoreContext } from '../../services/store';
import InstagramIcon from '../Icons/Instagram';
import TikTokIcon from '../Icons/TikTok';
import { onKeyDownEventCallback } from '../../utils/onKeyDown';

interface UserNameProps {
  color: string;
  username?: string;
  source?: string;
  postUrl?: string;
}

const UserName: FC<UserNameProps> = ({
  color,
  username: propsUsername,
  source: propsSource,
  postUrl: propsPostUrl,
}) => {
  const context = useContext(StoreContext);
  // Use context values if available, otherwise fall back to props
  const username = context.post?.username || propsUsername;
  const source = context.post?.source || propsSource;
  const postUrl = context.post?.post || propsPostUrl;

  const icon = {
    instagram: <InstagramIcon color={color} />,
    tiktok: <TikTokIcon color={color} />,
  }[source as 'instagram' | 'tiktok'];

  const onUsernameClick = (e: MouseEvent<HTMLAnchorElement> | KeyboardEvent<HTMLAnchorElement>) => {
    context.triggerEvent(
      'socialProfileOpened',
      {
        originalEvent: e.nativeEvent as Event,
        post: context.post!,
      },
      context.data.id,
    );
  };

  const userUrl = (): string | undefined => {
    if (!username) {
      return postUrl;
    }

    if (source === 'instagram' && postUrl) {
      return postUrl.slice(0, postUrl.indexOf('p/')) + username;
    }

    // TODO update this part (was Twitter before)
    if (source === 'tiktok' && postUrl) {
      return postUrl.slice(0, postUrl.indexOf('status/'));
    }
    return undefined;
  };

  return (
    <div
      className={`${context.config.classPrefix}-postviewer-details-username 
      ${pillClass(color)}`}
    >
      <span style={iconStyle}>{icon}</span>
      {username && (
        <a
          className={linkClass}
          href={userUrl()}
          target='_blank'
          onClick={onUsernameClick}
          onKeyDown={(e) => onKeyDownEventCallback(e, onUsernameClick)}
          tabIndex={0}
          role='link'
          aria-label={`go to ${username}'s profile on ${source}`}
        >
          <b className={textClass}>{username}</b>
        </a>
      )}
    </div>
  );
};

const pillClass = (color: string) =>
  style({
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    backdropFilter: 'blur(3px)',
    '-webkit-backdrop-filter': 'blur(3px)',
    padding: '6px 12px',
    borderRadius: '99px',
    color,
  });
const textClass = style({
  fontFamily: "'Fustat', sans-serif",
  fontWeight: 300,
  marginRight: 4,
  all: 'revert',
  fontSize: 12,
});
const iconStyle: CSSProperties = {
  verticalAlign: 'middle',
  marginRight: 5,
  marginBottom: 15,
};
const linkClass = style({
  textDecoration: 'inherit',
  color: 'inherit',
  $nest: {
    '&:hover': {
      color: 'gray',
    },
  },
});

export default UserName;
