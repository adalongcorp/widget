declare const ENV: {
  API_URL?: string;
  GA_KEY?: string;
};

export const API_URL = ENV.API_URL || 'https://api.adalong.com';
export const { GA_KEY } = ENV;
