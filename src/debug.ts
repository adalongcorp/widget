// Define debug levels with const assertion
const LEVELS = ['error', 'debug'] as const;

export type Level = (typeof LEVELS)[number];

export class Debug {
  private static level: number = 0;

  public static log(label: string, ...args: unknown[]): void {
    if (Debug.level >= LEVELS.indexOf('debug')) {
      /* eslint-disable no-console */
      console.debug(`[AdalongWidget] ${label}`, ...args);
    }
  }

  public static error(err: Error | string, ...args: unknown[]): void {
    /* eslint-disable no-console */
    console.error('[AdalongWidget]', err, ...args);
  }

  public static setLevel(level: Level): void {
    Debug.level = LEVELS.indexOf(level);
  }

  public static async try<T>(fn: () => T | Promise<T>): Promise<T | undefined> {
    try {
      const result = await fn();
      return result;
    } catch (error) {
      this.error(error instanceof Error ? error : String(error));
      return undefined;
    }
  }
}
