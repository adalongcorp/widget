import { KeyboardEvent } from 'react';

export const onKeyDownEventCallback = <T extends Element>(
  e: KeyboardEvent<T>,
  callback: (e: KeyboardEvent<T>) => void,
): void => {
  if (e.key === 'Enter' || e.key === ' ') {
    callback(e);
  }
};

export const onKeyDownCallback = <T extends HTMLOrSVGElement>(
  e: KeyboardEvent<T>,
  callback: () => void,
): void => {
  if (e.key === 'Enter' || e.key === ' ') {
    callback();
  }
};
