const formatCurrency = (price: number, currency: string)
: string => new Intl.NumberFormat(undefined, {
  style: 'currency',
  currency,
}).format(price);

export default formatCurrency;
