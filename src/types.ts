import * as React from 'react';
import { SyntheticEvent, ReactElement } from 'react';
import { useAdalongWidget } from './adalongWidget';
import { Level } from './debug';
import { StoreState } from './services/store';

declare global {
  interface Window {
    adalongWidgetAPI: AdalongWidgetAPI;
  }
}

/**
 * API exposed globally
 */
export interface AdalongWidgetAPI {
  widgets: ReturnType<typeof useAdalongWidget>[];
}

export interface DOMEvent {
  originalEvent: SyntheticEvent['nativeEvent'];
}

export interface PostEvent {
  post: IMedia;
}

export interface MediaPosition {
  position: number;
}

export interface ProductEvent {
  product: string;
}

export type Direction = 'left' | 'right';

export interface CarouselScrollEvent {
  position: number;
  direction: Direction;
}

export interface PostNavigationEvent {
  direction: Direction;
}

export interface MinContentEvent {
  required: number;
  current: number;
}

/**
 * Give information about the widget layout
 */
export interface LayoutEvent {
  layout: ISettings['type'];
}

export interface EventMap {
  // The widget has finished loading
  widgetLoaded: {
    elapsedSeconds?: number;
    posts: IMedia[];
  } & LayoutEvent;

  /**
   * The widget has been displayed to the user
   * This event is emitted only once when the widget
   * became visible in the viewport for at least 0.5 seconds
   */
  widgetViewed: LayoutEvent;

  widgetLoadingFailed: Record<string, never>;
  // A thumbnail has been loaded
  thumbnailLoaded: PostEvent & DOMEvent;

  // A thumbnail image could not be loaded
  thumbnailUnavailable: PostEvent & DOMEvent;

  // The mouse hovers a thumbnail
  thumbnailHover: PostEvent & DOMEvent;

  // A thumbnail has been clicked
  thumbnailClicked: PostEvent & DOMEvent & MediaPosition;

  // The original post has been opened thanks to a click on the content
  originalPostOpened: PostEvent & DOMEvent;

  // The social profile page has been opened thanks to a click on the username
  socialProfileOpened: PostEvent & DOMEvent;

  // The shop this look has been opened thanks to a click on the thumbnail
  shopThisLook: PostEvent & DOMEvent & ProductEvent;

  // A packshot has been clicked
  packshotClicked: PostEvent & DOMEvent & ProductEvent;

  // A post is opened with a thumbnail or a arrow
  postOpened: PostEvent;

  // An opened post has been closed
  postClosed: PostEvent;

  // Change the viewed post pressing left or right key or clicking on arrows
  postNavigation: PostNavigationEvent;

  // The video of the opened post is played
  videoPlayed: PostEvent & DOMEvent;

  // Scroll the carousel by using the arrows
  carouselArrowClicked: CarouselScrollEvent & DOMEvent;

  // Scroll the carousel by using the horizontal bar
  carouselNativeScroll: CarouselScrollEvent & DOMEvent;

  // Not enough content for the required min_media_to_display value
  // See ISettings in src/types.ts
  minContentNotReached: MinContentEvent;

  // Enough content for the required min_media_to_display value
  // See ISettings in src/types.ts
  minContentReached: MinContentEvent;
}

export type EventMapKeys = keyof EventMap;
export type EventCallbacks<T> = Array<EventCallback<T>>;
export type EventCallback<T> = (event: T) => void;
export interface IWidgetEvents {
  // Add a callback to listen to an event (previsouly added callbacks won't be overwritten)
  onEvent<EventName extends EventMapKeys>(
    eventName: EventName,
    callback: EventCallback<EventMap[EventName]>,
  ): void;

  // Remove a callback previously listening to an event
  offEvent<EventName extends EventMapKeys>(
    eventName: EventName,
    callback?: EventCallback<EventMap[EventName]>,
  ): void;
  triggerEvent<EventName extends EventMapKeys>(
    eventName: EventName,
    event: EventMap[EventName],
    widgetId?: string,
  ): void;
  hasSubscribed(eventName: EventMapKeys): boolean;
  destroy(): void;
}

export interface AdalongWidgetInstance {
  id: string;
  layout?: ISettings['type'];
  loaded: boolean;
  load: (element: string | HTMLElement, settings?: Partial<ISettings>) => Promise<void>;
  getSlideState: () => { canSlideLeft: boolean; canSlideRight: boolean };
  changePost: (dir: Direction) => void;
  setSlider: (dir: Direction) => void;
  setDebug: (debug: IDebug) => void;
  onEvent: IWidgetEvents['onEvent'];
  offEvent: IWidgetEvents['offEvent'];
  hasSubscribed: IWidgetEvents['hasSubscribed'];
  destroy: IWidgetEvents['destroy'];
}

export interface IDebug {
  level?: Level;
  apiUrl?: string;
}

export interface ISettings {
  // Define a localization (for catalog refs, display of language, link and price)
  localization?: string;

  // Only for wall widget, forces the number of columns to display
  forceColumns: number;

  /**
   * Minimum number of UGCs to display in the widget
   * If this constraint is not is not respected,
   * an minContentNotReached event will be emitted
   */
  min_media: number;

  // Maximum number of UGCs to display
  max_media: number | null;

  // Display only UGCs for which you have acquired the rights
  rights_agreed: boolean;

  //  Show only medias that have "diffusion" enabled
  diffusion?: boolean;

  //  Display UGCs that are linked to exact products
  has_exact_products: boolean;

  //  Display UGCs that reach the minimum amount of exact products linked
  min_exact_products: number;

  // Display UGCs that are linked to similar products
  has_similar_products: boolean;

  //  Display UGCs that reach the minimum amount of similar products linked
  min_similar_products: number;

  //  Define the widget layout type
  type: 'carousel' | 'wall' | 'mosaic';

  // Randomize the content order
  random: boolean;

  /**
   * Control the number of content in carousel and wall layouts
   * or the first content size in pixel in mosaic layouts
   */
  content_size: number;

  // Define the aspect ratio of the content
  thumbnail_shape: 'square' | 'portrait' | 'stories';

  // Display the creator's name on the content and on thumbnails
  display_name: boolean;

  // Choose how corners should be displayed on thumbnails
  thumbnail_corners: 'rounded' | 'square';

  // Display the products linked to the UGCs on the thumbnails
  thumbnail_packshot: boolean;

  // Display the products linked to the UGCs in a modal or in a list
  shop_this_look_display: 'modal' | 'product_list' | 'none';

  // Display only products that are "exact linked" in the STL
  display_exact_products_only: boolean;

  // Display only products that are "similar linked" in the STL
  display_similar_products_only: boolean;

  // Maximum number of exact linked products displayed per UGC in the STL
  max_exact_products_displayed: number | null;

  // Maximum number of similar linked products displayed per UGC in the STL
  max_similar_products_displayed: number | null;
}

export interface Config {
  /**
   * Key that should be unique on the page to represent the widget
   * Can be used to find the widget in the AdalongWidgetAPI
   */
  id?: string;

  /**
   * Class prefix for css styling
   * Default to adl-wdgt
   */
  classPrefix?: string;

  /**
   * Function to customize the query parameters in "shop this look" urls
   * It should return an object with parameter names as keys
   */
  shopThisLookHandler?: ShopThisLookHandler;

  /**
   * Function to customize the query parameters in "shop this look" urls
   * It should return an object with parameter names as keys
   */
  displayCustomShopThisLook?: DisplayCustomShopThisLook;

  /**
   * Custom text to display as title of the "Shop this look" section.
   * Defaults to 'SHOP THE LOOK'
   */
  shopThisLookText?: string;

  /**
   * Custom text to display as the undertitle of the
   * "More like this" section (similar linked products).
   * Defaults to 'More like this'
   */
  moreLikeThisText?: string;

  /**
   * Shop this look custom product buttons
   * This optional function can be used to generate custom buttons on the shopthislook products.
   */
  getCustomProductButtons?: GetCustomProductButtons;

  /**
   * This variable forces the display of carousel arrows in mobile mode
   * when it is set to true (where native scrolling would be the default behavior).
   */
  displayMobileCarouselArrows?: boolean;

  /**
   * This variable let the user hide the left and right arrows in carousel mode
   */
  hideArrows?: boolean;
  debug?: IDebug;
}

export interface Sources {
  productIds?: string[];
  collectionIds?: string[];
}

export interface IProduct {
  catalog_id: string;
  company_id: string;
  description: string;
  id: string;
  image_link: string;
  link: string;
  price?: number;
  currency?: string;
  title: string;
  updated_at: string;
  variant_name?: string;
}

export interface IMedia {
  id: string;
  type: 'image' | 'video' | 'text';
  caption: string;
  source: 'instagram' | 'tiktok';
  username?: string;
  post: string;
  image?: string;
  video?: string;
  thumbnail?: string;
  products?: IProduct[];
  productsLinked: {
    exact?: string[];
    similar?: string[];
  };
  postIndex: number;
}

export interface IContent {
  medias: IMedia[];
}

export interface IData {
  settings: ISettings;
  content: IContent;
  id: string;
}

export interface ImageSize {
  w: number;
  h: number;
}

export type ShopThisLookHandler = (
  source: { media: IMedia; product: IProduct },
  targetUrl: string,
) => void;

export type DisplayCustomShopThisLook = (props: {
  react: typeof React;
  products?: IProduct[];
  media: IMedia;
  isMobile: boolean;
  context: StoreState;
}) => ReactElement;

export type CustomProductButtonProps = {
  onClick: () => void;
};

export type CustomProductButton = (props: CustomProductButtonProps) => ReactElement;

export type CustomProductButtons = {
  topRight?: CustomProductButton;
  bottomRight?: CustomProductButton;
  topLeft?: CustomProductButton;
  bottomLeft?: CustomProductButton;
};

export type GetCustomProductButtons = (
  react: typeof React,
  product: IProduct,
  media: IMedia,
) => CustomProductButtons;
