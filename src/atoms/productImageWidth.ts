import { atom } from 'jotai';

const baseAtom = atom<Record<string, number>>({});

export const productImageWidthAtom = (productId: string) => {
  const derivedAtom = atom(
    (get) => get(baseAtom)[productId] || 0,
    (get, set, newWidth: number) => {
      const current = get(baseAtom);
      set(baseAtom, { ...current, [productId]: newWidth });
    },
  );
  return derivedAtom;
};
