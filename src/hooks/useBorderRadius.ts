/**
 * This hook calculates the border radius for the
 * thumbnail based on the width, height, and corner type.
 */
import { useMemo } from 'react';
import { ISettings } from '../types';

type CornerPosition = 'all' | 'top' | 'bottom' | 'left' | 'right';

const MIN_BORDER_RADIUS = 4;
const MAX_BORDER_RADIUS = 16;

const useBorderRadius = (
  width: number,
  height: number,
  cornerType: ISettings['thumbnail_corners'],
  position?: CornerPosition,
) => useMemo(() => {
  if (cornerType !== 'rounded') return '0px';

  const size = Math.min(width, height);
  const radius = Math.min(
    Math.max(size * 0.09, MIN_BORDER_RADIUS),
    MAX_BORDER_RADIUS,
  );

  const value = `${radius}px`;

  switch (position) {
    case 'top':
      return `${value} ${value} 0 0`;
    case 'bottom':
      return `0 0 ${value} ${value}`;
    case 'left':
      return `${value} 0 0 ${value}`;
    case 'right':
      return `0 ${value} ${value} 0`;
    default:
      return value;
  }
}, [width, height, cornerType, position]);

export default useBorderRadius;
