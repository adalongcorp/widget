import { useState, useEffect } from 'react';

/** Used to display the postviewer in a portal
 * Prevents CSS/layout conflicts with the parent component
 * Avoids z-index issues
 */

const usePortal = (id: string | undefined, shouldCreate: boolean) => {
  const [element, setElement] = useState<Element | null>(null);

  useEffect(() => {
    if (!id || !shouldCreate) {
      if (element) {
        element.remove();
        setElement(null);
      }
      return;
    }

    let portal = document.querySelector(`#${id}`);
    if (!portal) {
      portal = document.createElement('div');
      portal.id = id;
      document.body.appendChild(portal);
    }
    setElement(portal);

    return () => {
      if (portal && !shouldCreate) {
        portal.remove();
        setElement(null);
      }
    };
  }, [id, shouldCreate, element]);

  return element;
};

export default usePortal;
