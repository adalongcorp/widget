import { useEffect, useState, RefObject } from 'react';

const useVideoControls = (videoRef: RefObject<HTMLVideoElement>, videoUrl: string) => {
  const [isPaused, setIsPaused] = useState(true);

  useEffect(() => {
    videoRef.current?.load();
    setIsPaused(true);
  }, [videoUrl]);

  const playVideo = async () => {
    try {
      await videoRef.current?.play();
      setIsPaused(false);
    } catch (error) {
      setIsPaused(true);
      throw new Error('Error playing video. Please try again later.');
    }
  };

  const pauseVideo = () => {
    videoRef.current?.pause();
    setIsPaused(true);
  };

  return { isPaused, playVideo, pauseVideo };
};

export default useVideoControls;
