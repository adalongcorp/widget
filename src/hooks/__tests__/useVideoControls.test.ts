import { renderHook } from '@testing-library/react';
import { act } from 'react';
import useVideoControls from '../useVideoControls';

describe('useVideoControls', () => {
  let mockVideoElement: any;
  let mockVideoRef: any;

  beforeEach(() => {
    mockVideoElement = {
      load: jest.fn(),
      play: jest.fn().mockResolvedValue(undefined),
      pause: jest.fn(),
    };

    mockVideoRef = {
      current: mockVideoElement,
    };

    jest.clearAllMocks();
  });

  it('initializes with isPaused as true', () => {
    const { result } = renderHook(() => useVideoControls(mockVideoRef, 'test.mp4'));
    expect(result.current.isPaused).toBe(true);
  });

  it('loads video when URL changes', () => {
    const { rerender } = renderHook(({ url }) => useVideoControls(mockVideoRef, url), {
      initialProps: { url: 'video1.mp4' },
    });

    act(() => {
      rerender({ url: 'video2.mp4' });
    });

    expect(mockVideoElement.load).toHaveBeenCalled();
  });

  it('plays video and updates isPaused', async () => {
    const { result } = renderHook(() => useVideoControls(mockVideoRef, 'test.mp4'));

    await act(async () => {
      await result.current.playVideo();
    });

    expect(mockVideoElement.play).toHaveBeenCalled();
    expect(result.current.isPaused).toBe(false);
  });

  it('pauses video and updates isPaused', async () => {
    const { result } = renderHook(() => useVideoControls(mockVideoRef, 'test.mp4'));

    act(() => {
      result.current.pauseVideo();
    });

    expect(mockVideoElement.pause).toHaveBeenCalled();
    expect(result.current.isPaused).toBe(true);
  });

  it('handles play errors', async () => {
    mockVideoElement.play.mockRejectedValueOnce(new Error('Play failed'));
    const { result } = renderHook(() => useVideoControls(mockVideoRef, 'test.mp4'));

    try {
      await act(async () => {
        await result.current.playVideo();
      });
    } catch (error) {
      if (error instanceof Error) {
        expect(error.message).toBe('Error playing video. Please try again later.');
      }
    }

    expect(result.current.isPaused).toBe(true);
  });
});
