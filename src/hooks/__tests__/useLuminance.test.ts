import { renderHook } from '@testing-library/react';
import { act } from 'react';
import useLuminance from '../useLuminance';

describe('useLuminance', () => {
  let mockContext: any;
  let mockGetContext: jest.Mock;
  let mockCanvas: any;
  let mockImage: any;
  let createElementOriginal: any;

  beforeEach(() => {
    mockContext = {
      drawImage: jest.fn(),
      getImageData: jest.fn(),
    };
    mockGetContext = jest.fn(() => mockContext);
    mockCanvas = {
      getContext: mockGetContext,
      width: 0,
      height: 0,
      remove: jest.fn(), // Add remove method
    };
    mockImage = {
      width: 100,
      height: 100,
      onload: null,
      onerror: null,
    };

    createElementOriginal = document.createElement;
    jest.spyOn(document, 'createElement').mockImplementation((element: string) => {
      if (element === 'canvas') return mockCanvas;
      if (element === 'img') return mockImage;
      return createElementOriginal.call(document, element);
    });
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('returns white by default when no image URL is provided', () => {
    const { result } = renderHook(() => useLuminance(undefined));
    expect(result.current).toBe('white');
  });

  it('returns white for dark background', async () => {
    mockContext.getImageData.mockReturnValue({
      data: [20, 20, 20, 255],
    });
  
    const { result } = renderHook(() => useLuminance('test.jpg', 0.9, 0.9, 0.5, true));
  
    act(() => {
      const imgOnLoad = (document.createElement('img') as any).onload;
      if (imgOnLoad) imgOnLoad();
    });
  
    expect(result.current).toBe('#ffffff');
  });
  
  it('returns black for light background', async () => {
    mockContext.getImageData.mockReturnValue({
      data: [200, 200, 200, 255],
    });
  
    const { result } = renderHook(() => useLuminance('test.jpg', 0.9, 0.9, 0.5, true));
  
    act(() => {
      const imgOnLoad = (document.createElement('img') as any).onload;
      if (imgOnLoad) imgOnLoad();
    });
  
    expect(result.current).toBe('#000000');
  });
  
  it('respects custom coordinates and threshold', async () => {
    mockContext.getImageData.mockReturnValue({
      data: [128, 128, 128, 255],
    });
  
    const { result } = renderHook(() => 
      useLuminance('test.jpg', 0.5, 0.5, 0.7, true)
    );
  
    act(() => {
      const imgOnLoad = (document.createElement('img') as any).onload;
      if (imgOnLoad) imgOnLoad();
    });
  
    expect(result.current).toBe('#ffffff');
  });
  it('handles image load errors gracefully', () => {
    const { result } = renderHook(() => useLuminance('invalid.jpg'));

    act(() => {
      const imgOnError = (document.createElement('img') as any).onerror;
      if (imgOnError) imgOnError();
    });

    expect(result.current).toBe('white');
  });
  it('does not calculate luminance when shouldCalculate is false', () => {
    const { result } = renderHook(() => useLuminance('test.jpg', 0.9, 0.9, 0.5, false));
    
    expect(mockContext.drawImage).not.toHaveBeenCalled();
    expect(mockContext.getImageData).not.toHaveBeenCalled();
    expect(result.current).toBe('white');
  });

  it('calculates luminance when shouldCalculate is true', () => {
    mockContext.getImageData.mockReturnValue({
      data: [200, 200, 200, 255],
    });

    const { result } = renderHook(() => useLuminance('test.jpg', 0.9, 0.9, 0.5, true));

    act(() => {
      const imgOnLoad = (document.createElement('img') as any).onload;
      if (imgOnLoad) imgOnLoad();
    });

    expect(mockContext.drawImage).toHaveBeenCalled();
    expect(mockContext.getImageData).toHaveBeenCalled();
    expect(result.current).toBe('#000000');
  });

  it('updates calculation when shouldCalculate changes', () => {
    mockContext.getImageData.mockReturnValue({
      data: [200, 200, 200, 255],
    });

    const { result, rerender } = renderHook(
      ({ shouldCalc }) => useLuminance('test.jpg', 0.9, 0.9, 0.5, shouldCalc),
      { initialProps: { shouldCalc: false } }
    );

    expect(result.current).toBe('white');

    rerender({ shouldCalc: true });

    act(() => {
      const imgOnLoad = (document.createElement('img') as any).onload;
      if (imgOnLoad) imgOnLoad();
    });

    expect(result.current).toBe('#000000');
  });
});
