import { renderHook } from '@testing-library/react';
import useBorderRadius from '../useBorderRadius';

describe('useBorderRadius', () => {
  it('returns "0px" when cornerType is not rounded', () => {
    const { result } = renderHook(() => useBorderRadius(100, 100, 'square'));
    expect(result.current).toBe('0px');
  });

  it('returns correct radius for all corners', () => {
    const { result } = renderHook(() => useBorderRadius(100, 100, 'rounded'));
    expect(result.current).toBe('9px');
  });

  it('returns correct radius for top position', () => {
    const { result } = renderHook(() => useBorderRadius(100, 100, 'rounded', 'top'));
    expect(result.current).toBe('9px 9px 0 0');
  });

  it('returns correct radius for bottom position', () => {
    const { result } = renderHook(() => useBorderRadius(100, 100, 'rounded', 'bottom'));
    expect(result.current).toBe('0 0 9px 9px');
  });

  it('returns correct radius for left position', () => {
    const { result } = renderHook(() => useBorderRadius(100, 100, 'rounded', 'left'));
    expect(result.current).toBe('9px 0 0 9px');
  });

  it('returns correct radius for right position', () => {
    const { result } = renderHook(() => useBorderRadius(100, 100, 'rounded', 'right'));
    expect(result.current).toBe('0 9px 9px 0');
  });

  it('respects minimum border radius', () => {
    const { result } = renderHook(() => useBorderRadius(20, 20, 'rounded'));
    expect(result.current).toBe('4px');
  });

  it('respects maximum border radius', () => {
    const { result } = renderHook(() => useBorderRadius(300, 300, 'rounded'));
    expect(result.current).toBe('16px');
  });
});
