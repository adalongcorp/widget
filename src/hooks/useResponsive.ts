import { useEffect } from 'react';

const useResponsive = (root: HTMLElement, onResize: (width: number) => void) => {
  useEffect(() => {
    if (!root) return;

    const observer = new ResizeObserver((entries) => {
      entries.forEach((entry) => {
        onResize(Math.floor(entry.contentRect.width));
      });
    });

    observer.observe(root);
    // eslint-disable-next-line consistent-return
    return () => observer.disconnect();
  }, [root, onResize]);
};

export default useResponsive;
