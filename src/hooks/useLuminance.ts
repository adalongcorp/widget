/* eslint-disable max-len */
import { useState, useEffect } from 'react';

/**
 * This hook calculates the luminance of a certain area in the image and returns the
 * appropriate text color based on the threshold.
 */

const useLuminance = (
  imageUrl: string | undefined,
  x: number = 0.9,
  y: number = 0.9,
  threshold = 0.5,
  shouldCalculate = false // New parameter to control when to calculate
) => {
  const [textColor, setTextColor] = useState<string>('white');

  useEffect(() => {
    if (!imageUrl || !shouldCalculate) return;

    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const img = new Image();

    const calculateLuminance = (r: number, g: number, b: number) => 
      (0.299 * r + 0.587 * g + 0.114 * b) / 255;

    img.crossOrigin = 'Anonymous';
    img.src = imageUrl;

    img.onload = () => {
      canvas.width = img.width;
      canvas.height = img.height;

      ctx?.drawImage(img, 0, 0);

      const sampleX = Math.floor(img.width * x);
      const sampleY = Math.floor(img.height * y);
      const [r, g, b] = ctx?.getImageData(sampleX, sampleY, 1, 1).data || [0, 0, 0];

      const luminance = calculateLuminance(r, g, b);
      setTextColor(luminance > threshold ? '#000000' : '#ffffff');
    };

    return () => {
      img.onload = null;
      canvas.remove();
    };
  }, [imageUrl, x, y, threshold, shouldCalculate]);

  return textColor;
};

export default useLuminance;
