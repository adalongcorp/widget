import * as React from 'react';
import {
  Dispatch,
  useState,
  useEffect,
  ReactNode,
  SetStateAction,
  createContext,
  useContext,
  FC,
} from 'react';
import { Config, IData, IMedia, IWidgetEvents } from '../types';

import { Debug } from '../debug';

export const DEFAULT_CONFIG: Config = {
  classPrefix: 'adl-wdgt',
  shopThisLookText: 'SHOP THE LOOK',
  moreLikeThisText: 'More like this',
};

export interface StoreState {
  exportState: (getState: () => StoreState, setter: StateSetter) => void;
  post: IMedia | null;
  data: IData;
  forceMobile: boolean;
  isMobile: boolean;
  sliderPosition: number;
  root: Element | null;
  config: Config;
  triggerEvent: IWidgetEvents['triggerEvent'];
  hasSubscribed: IWidgetEvents['hasSubscribed'];
  setStoreState: Dispatch<SetStateAction<StoreState>>;
  canSlideLeft: boolean;
  canSlideRight: boolean;
}

export interface StoreProps {
  state: Partial<StoreState>;
  children: ReactNode;
}

export const initial: StoreState = {
  exportState: () => {},
  post: null,
  data: {} as IData,
  forceMobile: false,
  isMobile: false,
  root: null,
  sliderPosition: 0,
  config: DEFAULT_CONFIG,
  triggerEvent: () => {},
  hasSubscribed: () => false,
  setStoreState: () => {},
  canSlideLeft: false,
  canSlideRight: true,
};

export const StoreContext = createContext<StoreState>(initial);

export type StateSetter = Dispatch<SetStateAction<StoreState>>;

export const useStore = () => useContext(StoreContext);

interface ErrorBoundaryProps {
  children: ReactNode;
}

const ErrorBoundary: FC<ErrorBoundaryProps> = ({ children }) => {
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    const errorHandler = (event: ErrorEvent) => {
      Debug.error(event.error as Error);
      setHasError(true);
    };

    window.addEventListener('error', errorHandler);
    return () => window.removeEventListener('error', errorHandler);
  }, []);

  if (hasError) {
    return null;
  }

  return <>{children}</>;
};

export const StoreProvider: FC<StoreProps> = ({ state, children }) => {
  const [storeState, setStoreState] = useState<StoreState>(() => {
    // Create initial state first
    const initialState = {
      ...initial,
      ...state,
    };

    // Then add setter function
    return {
      ...initialState,
      setStoreState: (newState: SetStateAction<StoreState>) => {
        setStoreState(
          typeof newState === 'function' ? newState : (current) => ({ ...current, ...newState }),
        );
      },
    };
  });

  // Move effects after state initialization
  useEffect(() => {
    if (state.exportState) {
      state.exportState(() => storeState, setStoreState);
    }
  }, [state.exportState, storeState]);

  useEffect(() => {
    setStoreState((current) => ({
      ...current,
      ...state,
    }));
  }, [state]);

  return (
    <ErrorBoundary>
      <StoreContext.Provider value={storeState}>{children}</StoreContext.Provider>
    </ErrorBoundary>
  );
};

// Hook for components to consume store
export const useStoreComponent = <P extends object>(props: P): [P, StoreState, StateSetter] => {
  const store = useStore();
  return [props, store, store.setStoreState];
};
