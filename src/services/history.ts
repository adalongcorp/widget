import { IMedia } from '../types';

const HistoryService = {
  push: (post: IMedia): void => {
    window.history.pushState({ widgetPost: post }, `widgetpost-${post.postIndex}`, null);
  },

  replace: (post: IMedia): void => {
    window.history.replaceState({ widgetPost: post }, `widgetpost-${post.postIndex}`, null);
  },
};

export default HistoryService;
