import { Helpers } from '../helpers';

describe('helpers', () => {
  it('should split array into chunks', () => {
    const arr = [1, 2, 3, 4, 5, 6, 7, 8];
    const chunks = Helpers.chunkArray(arr, 3);
    expect(chunks).toStrictEqual([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8],
    ]);
  });
  it('should return only one chunk', () => {
    const arr = [1, 2, 3, 4, 5, 6, 7, 8];
    const chunks = Helpers.chunkArray(arr, 10);
    expect(chunks).toStrictEqual([[1, 2, 3, 4, 5, 6, 7, 8]]);
  });
  it('should return an empty array', () => {
    const arr: any[] = [];
    const chunks = Helpers.chunkArray(arr, 10);
    expect(chunks).toStrictEqual([]);
  });
});
