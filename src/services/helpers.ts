import { IMedia } from '../types';
import HistoryService from './history';

export const MOBILE_WIDTH_THRESHOLD = 740;

export const Helpers = {
  createArray: (length: number): number[] => Array.from({ length }),

  // eslint-disable-next-line max-len
  chunkArray: <T>(array: T[], chunkSize: number): T[][] => Array.from({ length: Math.ceil(array.length / chunkSize) }, (_, i) => array.slice(i * chunkSize, (i + 1) * chunkSize)),

  getAbsoluteBackgroundColor: (element?: Element | null): string => {
    if (!element) {
      return 'white';
    }

    const bg = window.getComputedStyle(element).getPropertyValue('background-color');

    if (bg === 'transparent' || bg === 'rgba(0, 0, 0, 0)') {
      return element.parentElement
        ? Helpers.getAbsoluteBackgroundColor(element.parentElement)
        : 'white';
    }

    return bg;
  },

  isValidJson: (jsonString: string): boolean => {
    try {
      const parsed : unknown = JSON.parse(jsonString);
      return Boolean(parsed) && typeof parsed === 'object';
    } catch {
      return false;
    }
  },

  isMobile: (width: number): boolean => width < MOBILE_WIDTH_THRESHOLD,

  pushPostToHistory: (post: IMedia): void => {
    HistoryService.push(post);
  },

  replacePostInHistory: (post: IMedia): void => {
    HistoryService.replace(post);
  },

  uniqId: (): string => `${Date.now()}${Math.floor(Math.random() * 1000)}`,

  randomId: (): string => Math.random().toString(36).slice(2, 10),
};
