const path = require('path');
const webpack = require('webpack');
const ESLintPlugin = require('eslint-webpack-plugin');
const dotenv = require('dotenv');

const env = dotenv.config().parsed;

module.exports = {
  target: 'web',
  mode: env.NODE_ENV === 'production' ? 'production' : 'development',
  devtool: 'source-map',
  entry: {
    app: [path.resolve(__dirname, 'src/adalongWidget.tsx')],
  },

  output: {
    library: 'AdalongWidget', // Global variable name for CDN
    libraryTarget: 'umd', // Universal Module Definition
    filename: 'adalongWidget.js',
    umdNamedDefine: true,
    globalObject: 'this', // Ensures it works in all environments
  },

  resolve: {
    modules: [path.resolve('./node_modules')],
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    mainFields: ['browser', 'module', 'main'],
  },

  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
          },
        ],
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      ENV: JSON.stringify({
        API_URL: env.API_URL,
        GA_KEY: env.GA_KEY,
      }),
    }),
    new ESLintPlugin(),
  ],

  devServer: {
    static: {
      directory: path.join(__dirname, 'dist'),
    },
    hot: true,
    port: 9800,
  },
};
