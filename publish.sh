#!/bin/sh

set -xe

npm version $1

git push --follow-tags

npm publish
