import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    video: false,
    experimentalWebKitSupport: true,
    setupNodeEvents(on, config) {},
    defaultCommandTimeout: 10000,
  },
});
